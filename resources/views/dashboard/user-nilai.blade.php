@extends('dashboard.template')


@section('title', 'Daftar Nilai')


@section('konten')
		<ul class="app-menu">
			<li><a href="{{ route('dashboard.index') }}" class="app-menu__item "><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li><a href="{{ route('dashboard.ulangan') }}" class="app-menu__item"><i class="app-menu__icon fa fa-external-link"></i><span class="app-menu__label">Ikuti Ulangan</span></a></li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item active"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-edit"></i> Daftar Nilai Ulangan</h1>
				<p>Cek hasil ulanganmu disini</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">
						<span class="mr-4">Hasil Ulangan</span>
						<button class="btn btn-sm btn-primary ml-4" id="export" onclick="exportNilai({{ $id }})">Export Nilai</button>
					</div>
					<div class="tile-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th>Judul Ulangan</th>
									<th>Perolehan Nilai</th>
									<th>KKM</th>
									<th>Status</th>
									<th>Review</th>
								</tr>
							</thead>
							<tbody>
								@if (isset($nilai))
									@foreach ($nilai as $item)
										<tr>
											<td scope="row">{{ title_case(str_limit($item->exam_name, 20)) }}</td>
											<td>{{ $item->point }}</td>
											<td>{{ $item->kkm }}</td>
											<td>{{ $item->status }}</td>
											<td>
												<a href="{{ url('/ulangan/review/'.$item->exam_id) }}" class="btn btn-sm btn-info">Review</a>
											</td>
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection

@section('jscript')
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script>
		function exportNilai(e) {
			window.location.href = "{{ url('dashboard/nilai/export') }}?id=" + e;
		}

		$(document).ready(function () {
			$('#table').DataTable();
		});
	</script>
@endsection
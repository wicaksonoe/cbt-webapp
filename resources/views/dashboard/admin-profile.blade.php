@extends('dashboard.template')


@section('title', 'Profile User')


@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item " href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item "><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="row user">
			<div class="col-md-12">
				<div class="profile">
					<div class="info">
						<img class="user-img" src="{{ $photo_profile }}">
						<h4>{{ $firstName }} {{ $lastName }}</h4>
						<p>{{ $kelas }} - {{ $posisi }}</p>
					</div>
					<div class="cover-image" style="background-image: url({{ asset($photo_cover) }})"></div>
				</div>
			</div>
			<div class="col-12">
				<div class="tab-content">
					<div class="tab-pane active" id="user-settings">
						<div class="tile user-settings">
							<h4 class="line-head">Ubah Profile</h4>
							@if ($errors->any())
								<div class="alert alert-dismissible alert-danger">
									<button class="close" type="button" data-dismiss="alert">×</button>
									<strong>Error</strong>
									@foreach ($errors->all() as $error)										
										<p class="mb-0">{{ $error }}</p>
									@endforeach
								</div>
							@endif
							<form action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
								@method('PUT')
								@csrf
								<div class="row mb-4">
									<div class="col-md-4">
										<label>First Name</label>
									<input class="form-control" type="text" name="name_first" placeholder="{{ $firstName }}">
									</div>
									<div class="col-md-4">
										<label>Last Name</label>
										<input class="form-control" type="text" name="name_last" placeholder="{{ $lastName }}">
									</div>
								</div>
								<div class="row">
									<div class="col-md-8 mb-4">
										<label>Username</label>
									<input class="form-control" type="text" name="username" readonly="readonly" placeholder="{{ $username }}">
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 mb-4">
										<label>Posisi</label>
										<input class="form-control" type="text" name="kelas" placeholder="{{ $kelas }}">
									</div>
									<div class="col-md-4 mb-4">
										<label>Mata Pelajaran</label>
										<input class="form-control" type="text" name="posisi" placeholder="{{ $posisi }}">
									</div>
								</div>
								<br><br>
								<h4 class="line-head">Ubah Password</h4>
								<div class="row">
									<div class="col-4 mb-4">
										<input class="form-control" type="password" name="password" id="password">
										<small class="form-text text-muted">New Password</small>
									</div>
									<div class="col-4 mb-4 align-self-end">
										<input type="password" class="form-control" name="re_password" id="re_password">
										<small class="form-text text-muted">Confirm Password</small>
									</div>
								</div>
								<div class="row">
									<div class="o col-4 mb-4">
										<label>Ubah Foto Sampul</label>
										<input class="form-control-file" type="file" name="photo_cover">
									</div>
									<div class="col-4 mb-4">
										<label>Ubah Foto Profil</label>
										<input class="form-control-file" type="file" name="photo_profile">
										<small class="form-text text-muted">Lebih bagus apabila rasio foto 1:1</small>
									</div>
								</div>
								<div class="row mb-10">
									<div class="col-md-12">
										<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
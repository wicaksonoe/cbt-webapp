@extends('dashboard.template')


@section('title', 'Dashboard')


@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item active" href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
						Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-dashboard"></i> Dashboard</h1>
				<p>Lihat Ringkasan Ulangan Disini</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
						<h1 class="text-center">Selamat Datang</h1>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
@extends('dashboard.template')


@section('title', 'Dashboard')


@section('konten')
	<ul class="app-menu">
		<li><a href="{{ route('dashboard.index') }}" class="app-menu__item active"><i class="app-menu__icon fa fa-dashboard"></i><span
				 class="app-menu__label">Dashboard</span></a></li>
		<li><a href="{{ route('dashboard.ulangan') }}" class="app-menu__item"><i class="app-menu__icon fa fa-external-link"></i><span class="app-menu__label">Ikuti
					Ulangan</span></a></li>
		<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
					Nilai</span></a></li>
	</ul>
</aside>
<main class="app-content">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-dashboard"></i> Dashboard</h1>
			<p>Lihat Ringkasan Ulangan Disini</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="tile">
				<h1 class="text-center">Selamat Datang</h1>
			</div>
		</div>
	</div>
</main>
@endsection
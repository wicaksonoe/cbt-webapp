<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
</head>

<body class="app sidebar-mini rtl">
	@yield('modal')
	<!-- Navbar-->
	<header class="app-header"><a class="app-header__logo" href="{{ route('dashboard.index') }}" style="font-family: sans-serif"><strong>CBT</strong></a>
		<!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
		<!-- Navbar Right Menu-->
		<ul class="app-nav">
			<!-- User Menu-->
			<li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i
					 class="fa fa-user fa-lg"></i></a>
				<ul class="dropdown-menu settings-menu dropdown-menu-right">
					<li><a class="dropdown-item" href="{{ route('profile.index') }}"><i class="fa fa-user fa-lg"></i> Profile</a></li>
					<li><a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
				</ul>
			</li>
		</ul>
	</header>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
	<aside class="app-sidebar">
		<div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{ $small_profile }}"
				alt="User Image">
			<div>
				<p class="app-sidebar__user-name">{{ $firstName }} {{ $lastName }}</p>
				<p class="app-sidebar__user-designation">{{ $kelas }} - {{ $posisi }}</p>
			</div>
		</div>
	@yield('konten')

	<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>

	@yield('jscript')
</body>

</html>
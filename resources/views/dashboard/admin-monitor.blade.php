@extends('dashboard.template')


@section('title', 'Monitor Ulangan')

@section('modal')

@endsection

@section('konten')
		<ul class="app-menu">
			<li>
				<a class="app-menu__item " href="{{ route('dashboard.index') }}">
					<i class="app-menu__icon fa fa-dashboard"></i>
					<span class="app-menu__label">Dashboard</span>
				</a>
			</li>
			<li class="treeview is-expanded">
				<a class="app-menu__item" href="#" data-toggle="treeview">
					<i class="app-menu__icon fa fa-laptop"></i>
					<span class="app-menu__label">Kelola Ulangan</span>
					<i class="treeview-indicator fa fa-angle-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item active"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-laptop"></i> Daftar Peserta Ulangan</h1>
				<p>Halaman untuk memantau peserta ulangan.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">Pilih Ulangan</div>
					<div class="tile-body">
						<div class="row">
							<div class="form-group col-6">
								<label for="exam_name">Judul Ulangan</label>
								<select onchange="monitoring($(this).val())" name="exam_name" id="exam_name" class="form-control">
									<option value="x">--Pilih Ulangan--</option>
									@foreach ($ulangan as $item)
										<option value="{{ $item->id }}">{{ $item->exam_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">
						Daftar Peserta Ulangan
						<a href="#" onclick="refresh()" class="ml-4">
							<i class="fa fa-refresh" aria-hidden="true"></i>
						</a>
					</div>
					<div class="tile-body">
						<div class="overlay d-none" id="load_content">
							<div class="m-loader mr-4">
								<svg class="m-circular" viewBox="25 25 50 50">
									<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
								</svg>
							</div>
							<h3 class="l-text">Loading</h3>
						</div>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th style="width: 5%">Kelas</th>
									<th style="width: 15%">No. Absen</th>
									<th style="width: 60%">Nama</th>
									<th style="width: 20%">Aksi</th>
								</tr>
							</thead>
							<tbody id="daftar">
		
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection


@section('jscript')
	<!-- The javascript plugin to display page loading on top-->
	<script src={{ asset("js/plugins/pace.min.js") }}></script>
	<!-- Page specific javascripts-->
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script src={{ asset('js/plugins/sweetalert.min.js') }}></script>
	<script>
		function monitoring(el) {
			$('#table').DataTable().destroy();
			$('#daftar').children().remove();
			$.ajax({
				method: "GET",
				url: "{{url('/dashboard/ulangan/monitor/get')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'id'	: el
				}
			}).done(function (e) {
				if(e.value == 1) {
					$.each(e.data, function(index, data) {
						var row = $(
							'<tr id="' + data.id + '">' +
								'<td class="kelas">' + data.kelas + "</td>" +
								'<td class="absen">' + data.absen + "</td>" +
								'<td class="nama">' + data.nama + "</td>" +
								"<td>" + 
									'<button onclick="forceDoneConfirm(this)" value="' + data.id + '" class="btn btn-sm btn-danger">Keluarkan</button>' +
								"</td>" +
							"</tr>"
						);
						$('#daftar').append(row);
					});
				}
				$('#table').DataTable();
			});
		}

		function refresh() {
			var id = $('#exam_name').val();
			if(id != 'x') {
				monitoring(id);
			} else {
				swal('Error', 'Silahkan pilih ulangan terlebih dahulu.', 'error');
			}
		}

		function forceDone(el) {
			$('#table').DataTable().destroy();
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/ulangan/monitor')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'user_id'	: $(el).val(),
					'exam_id'	: $('#exam_name').val()
				}
			}).done(function (e) {
				if(e.value == 1) {
					$('#daftar').children('#' + e.data.id).remove();
				}
				$('#table').DataTable();
			});
		}
		
		function forceDoneConfirm(el) {
			var id = $(el).val();
			swal({
				title: "Yakin akan menghentikan ulangan " + $('tr#' + id).children('.nama')[0].innerHTML + " ?",
				text: "Kelas : " + $('tr#' + id).children('.kelas')[0].innerHTML,
				type: "warning",
				showCancelButton: true,
				confirmButtonText: "Yakin",
				cancelButtonText: "Batal",
				closeOnConfirm: true,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {
					forceDone(el);
				} else {
					swal("Batal", "Ulangan batal dihentikan.", "error");
				}
			});
		}

		$(document).ready(function() {
			$('#table').DataTable();
		});
	</script>
@endsection
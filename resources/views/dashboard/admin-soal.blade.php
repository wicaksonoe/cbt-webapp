@extends('dashboard.template')


@section('title', 'Daftar Soal')

@section('modal')
	{{-- Modal Overlay --}}
	<div class="modal fade" id="soal_detail-overlay" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="soal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalCenterTitle">Detail & Edit Soal</h5>
			<button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="soal_id-edit" name="id" value="">
				<div class="form-horizontal">
					<div class="form-group row">
						<label class="control-label col-2 align-self-end" for="exam_name-edit">Ulangan</label>
						<div class="col-6">
							<input type="text" id="exam_name-edit" class="form-control" value="" readonly>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-12">
						<label for="question-edit">Soal</label>
						<textarea name="question" id="question-edit" cols="30" rows="3" class="form-control"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-6 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">A.</div>
							</div>
							<input type="text" name="opt_a" id="opt_a-edit" class="form-control">
						</div>
					</div>
					<div class="col-6 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">C.</div>
							</div>
							<input type="text" name="opt_c" id="opt_c-edit" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-6 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">B.</div>
							</div>
							<input type="text" name="opt_b" id="opt_b-edit" class="form-control">
						</div>
					</div>
					<div class="col-6 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">D.</div>
							</div>
							<input type="text" name="opt_d" id="opt_d-edit" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-6 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">Kunci Jawaban</div>
							</div>
							<select name="opt_ok" id="opt_ok-edit" class="form-control">
								<option value=""></option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<option value="D">D</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 form-group">
						<button type="button" id="hapusSoal" class="btn btn-danger">Hapus Soal</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			<button type="button" class="closeButton btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" onclick="updateSoal($('#soal_id-edit').val())">Save changes</button>
			</div>
		</div>
		</div>
	</div>
@endsection

@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item " href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview is-expanded"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item active"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
						Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-laptop"></i> Daftar Soal Ulangan</h1>
				<p>Silahkan membuat soal ulangan</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">Lihat Soal Ulangan</div>
					<div class="tile-body">
						@if ($errors->any())
							<div class="alert alert-dismissible alert-danger">
								<button class="close" type="button" data-dismiss="alert">×</button>
								<strong>Error</strong>
								@foreach ($errors->all() as $error)										
									<p class="mb-0">{{ $error }}</p>
								@endforeach
							</div>
						@endif
						<div class="row">
							<div class="form-group col-6">
								<label for="exam_name">Judul Ulangan</label>
								<select onchange="getSoal($(this).val())" name="exam_name" id="exam_name" class="form-control">
									<option value="x">--Pilih Ulangan--</option>
									@foreach ($ulangan as $item)
										<option value="{{ $item->id }}">{{ $item->exam_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-2 align-self-end">
								<button class="btn btn-primary" id="previewSoal">Preview Soal</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="tile">
					<div class="tile-title">
						<span class="mr-4">Tambah Soal</span>
						<form action="{{ url('/dashboard/soal/import') }}" method="post" class="d-none" enctype="multipart/form-data">
							@method('POST')
							@csrf
							<input type="file" class="d-none" name="import_soal" id="import_soal" onchange="$('#submit_upload').click()">
							<button type="submit" class="d-none" id="submit_upload"></button>
						</form>
						<button onclick="$('#import_soal').click()" class="btn btn-sm btn-success ml-4" id="import_button">Import Soal</button>
						<button onclick="exportSoal()" class="btn btn-sm btn-info ml-4" id="soal_export">Export Soal</button>
						<button class="btn btn-primary btn-sm float-right align-self-end" id="soal_show">show</button>
					</div>
					<div class="tile-body d-none" id="soal_body">
						<div class="alert alert-danger" style="display: none">
							<button class="close" type="button" onclick="$('.alert-danger').hide();">×</button>
							<strong>Error!</strong>
							<div class="message">
							</div>
						</div>
						<div class="alert alert-success" style="display: none">
							<button class="close" type="button" onclick="$('.alert-success').hide();">×</button>
							<strong>Sukses</strong>
							<p class="mb-0">Soal berhasil dibuat.</p>
						</div>
						<input type="hidden" id="exam_id" name="exam_id" value="">
						<div class="form-horizontal">
							<div class="form-group row">
								<label class="control-label col-1 align-self-end" for="exam_name_">Ulangan</label>
								<div class="col-6">
									<input type="text" id="exam_name_" class="form-control" value="" readonly>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label for="question">Soal</label>
								<textarea name="question" id="question" cols="30" rows="3" class="form-control" tabindex="1"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-6 form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">A.</div>
									</div>
									<input type="text" name="opt_a" id="opt_a" class="form-control" tabindex="2">
								</div>
							</div>
							<div class="col-6 form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">C.</div>
									</div>
									<input type="text" name="opt_c" id="opt_c" class="form-control" tabindex="3">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6 form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">B.</div>
									</div>
									<input type="text" name="opt_b" id="opt_b" class="form-control" tabindex="2">
								</div>
							</div>
							<div class="col-6 form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">D.</div>
									</div>
									<input type="text" name="opt_d" id="opt_d" class="form-control" tabindex="4">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6 form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">Kunci Jawaban</div>
									</div>
									<select name="opt_ok" id="opt_ok" class="form-control" tabindex="5">
										<option value=""></option>
										<option value="A">A</option>
										<option value="B">B</option>
										<option value="C">C</option>
										<option value="D">D</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 form-group">
								<button type="button" onclick="tambahSoal($('#exam_id').val())" class="btn btn-primary" tabindex="6"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Soal</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">
						Daftar Soal
						<a href="javascript:void(0)" onclick="getSoal($('#exam_name').val())" class="ml-4">
							<i class="fa fa-refresh" aria-hidden="true"></i>
						</a>
					</div>
					<div class="tile-body">
						<div class="overlay d-none" id="load_content">
							<div class="m-loader mr-4">
								<svg class="m-circular" viewBox="25 25 50 50">
									<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
								</svg>
							</div>
							<h3 class="l-text">Loading</h3>
						</div>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Soal</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody id="daftarSoal">
		
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection


@section('jscript')
	<!-- The javascript plugin to display page loading on top-->
	<script src={{ asset("js/plugins/pace.min.js") }}></script>
	<!-- Page specific javascripts-->
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script src={{ asset('js/plugins/sweetalert.min.js') }}></script>
	<script>
		function tambahSoal(id) {
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/soal/store')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'exam_id'	: $('#exam_id').val(),
					'question'	: $('#question').val(),
					'opt_a'		: $('#opt_a').val(),
					'opt_b'		: $('#opt_b').val(),
					'opt_c'		: $('#opt_c').val(),
					'opt_d'		: $('#opt_d').val(),
					'opt_ok'	: $('#opt_ok').val(),
				}
			}).done(function(e) {
				$('#question').val("");
				$('#opt_a').val("");
				$('#opt_b').val("");
				$('#opt_c').val("");
				$('#opt_d').val("");
				if(e.value == 0) {
					$('.alert-danger').children('div.message').children().remove();
					$.each(e.errors, function(key, val) {
						$('.alert-danger').children('div.message').append('<p class="mb-0">' + val + '</p>');
					});
					$('.alert-danger').show();
					$('.alert-success').hide();
				}else if(e.value == 1) {
					$('.alert-danger').hide();
					$('.alert-success').show();
					getSoal(e['data']['exam_id']);
				}
			});
		}
		
		function getSoal(el) {
			$('#table').DataTable().destroy();
			$('#load_content').toggleClass('d-none');
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/soal/load')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'id': el
				}
			}).done(function(e) {
				$('#exam_id').val($('#exam_name').val());
				$('#exam_name_').val($('option[value="'+ $('#exam_id').val() +'"]')[0].innerHTML);
				$('#exam_name-edit').val($('option[value="'+ $('#exam_id').val() +'"]')[0].innerHTML);
				$('#daftarSoal').children().remove();
				if(e['status'] == 'success') {
					$.each(e['data'], function(index, data) {
						var row = $(
							"<tr>" +
								"<td>" + (index + 1) + "</td>" +
								"<td>" + data.question + "</td>" +
								"<td>" + 
									'<button onclick="editSoal(this)" value="' + data.id + '" class="edit btn btn-sm btn-info">View Detail</button>' +
								"</td>" +
							"</tr>"
						);
						$('#daftarSoal').append(row);
					});
				}
				$('#load_content').toggleClass('d-none');
				$('#table').DataTable();
				$('.fa-refresh').removeClass('fa-spin');
			});
		}

		function editSoal(el) {
			$('#soal_detail-overlay').modal({backdrop: 'static', keyboard: false});
			$('#soal_detail-overlay').modal('show');
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/soal/edit')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'id'	: el.value
				}
			}).done(function(e) {
				if (e.value == 1) {
					$('#question-edit').attr('placeholder', e.data.question);
					$('#opt_a-edit').attr('placeholder', e.data.opt_a);
					$('#opt_b-edit').attr('placeholder', e.data.opt_b);
					$('#opt_c-edit').attr('placeholder', e.data.opt_c);
					$('#opt_d-edit').attr('placeholder', e.data.opt_d);
					$('#soal_id-edit').val(e.data.id);
					$('#opt_ok-edit').children('option[value="' + e.data.opt_ok + '"]').prop('selected', true);
					$('#soal_detail').modal('show');
				}
			});
		}

		function updateSoal(el) {
			$.ajax({
				method: 'PUT',
				url: "{{ url('/dashboard/soal/update') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'id': el,
					'question': $('#question-edit').val(),
					'opt_a': $('#opt_a-edit').val(),
					'opt_b': $('#opt_b-edit').val(),
					'opt_c': $('#opt_c-edit').val(),
					'opt_d': $('#opt_d-edit').val(),
					'opt_ok': $('#opt_ok-edit').val()
				}
			}).done(function(data) {
				if(data.value == 1) {
					swal("Berhasil!", "Soal berhasil diubah.", "success");
					$('#soal_detail-overlay').modal('hide');
					$('#soal_detail').modal('hide');
					getSoal($('#exam_name').val());
				} else {
					swal("Error!", "Terjadi kesalahan, silahkan coba lagi.", "error");
					$('#soal_detail-overlay').modal('hide');
					$('#soal_detail').modal('hide');
					getSoal($('#exam_name').val());
				}
				$('#question-edit').val("");
				$('#opt_a-edit').val("");
				$('#opt_b-edit').val("");
				$('#opt_c-edit').val("");
				$('#opt_d-edit').val("");
				$('#opt_ok-edit').val("");
			});
		}

		function hapusSoal(el) {
			$.ajax({
				method: "DELETE",
				url: "{{url('/dashboard/soal/delete')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'id'	: el
				}
			}).done(function (data) {
				if (data.value == 1) {
					swal("Terhapus!", "Soal berhasil dihapus.", "success");
					$('#soal_detail-overlay').modal('hide');
					$('#soal_detail').modal('hide');
					getSoal($('#exam_name').val());
				} else {
					swal("Error!", "Terjadi kesalahan saat menghapus soal, silahkan coba lagi.", "error");
					$('#soal_detail-overlay').modal('hide');
					$('#soal_detail').modal('hide');
					getSoal($('#exam_name').val());
				}
			});
		}

		function exportSoal() {
			var e = $('#exam_name');
			var data = {
				'id': e.val(),
				'exam_name': e.children('option[value="' + e.val() + '"]')[0].innerHTML,
			}
			if (data.id == "x") {
				swal("Error", "Silahkan pilih ulangan terlebih dahulu", "error");
			} else {
				window.location.href = "{{ url('/dashboard/soal/export') }}?id=" + data.id + "&exam_name=" + data.exam_name;
			}
		}

		$('.closeButton').click(function() {
			$('#soal_detail-overlay').modal('hide');
			$('#soal_detail').modal('hide');
		});

		$('#table').DataTable();

		$('#hapusSoal').click(function(){
			swal({
				title: "Yakin akan menghapus soal ini?",
				text: "Setelah dihapus, maka soal tidak dapat dikembalikan!",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: "Yakin",
				cancelButtonText: "Batal",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {
					hapusSoal($('#soal_id-edit').val());
				} else {
					swal("Batal", "Soal batal dihapus, dan tidak berubah", "error");
				}
			});
		});

		$('#soal_show').click(function () {
			if (this.innerHTML === "show") {
				this.innerHTML = "hide";
			} else {
				this.innerHTML = "show";
			}
			$('#soal_body').toggleClass('d-none');
		});

		$('.fa-refresh').click(function() {
			$(this).toggleClass('fa-spin');
		});
	</script>
@endsection
@extends('dashboard.template')


@section('title', 'Dashboard Nilai')


@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item " href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item active"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
						Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-edit"></i> Lihat Daftar Nilai Siswa</h1>
				<p>Cek perolehan nilai siswa</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">Cari Berdasarkan Ulangan</div>
					<div class="tile-body">
						<div class="row">
							<div class="form-group col-6">
								<label for="exam_name">Judul Ulangan</label>
								<select name="exam_name" id="exam_name" class="form-control" onchange="getNilai()">
									<option value="x">--Pilih Ulangan--</option>
									@foreach ($ulangan as $item)
										<option value="{{ $item->id }}">{{ $item->exam_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">
						<span>Daftar Nilai</span>
						<button onclick="exportNilai()" class="btn btn-sm btn-info ml-4" id="nilai_export">Export Nilai</button>
					</div>
					<div class="tile-body">
						<div class="overlay d-none" id="load_content">
							<div class="m-loader mr-4">
								<svg class="m-circular" viewBox="25 25 50 50">
									<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
								</svg>
							</div>
							<h3 class="l-text">Loading</h3>
						</div>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Kelas</th>
									<th>Nomor Absen</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody id="tableNilai">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection

@section('jscript')
	<!-- The javascript plugin to display page loading on top-->
	<script src={{ asset("js/plugins/pace.min.js") }}></script>
	<!-- Page specific javascripts-->
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script src={{ asset("js/plugins/sweetalert.min.js") }}></script>
	<script>
		$('#table').DataTable();

		function exportNilai() {
			var id = $('#exam_name').val();
			var exam_name = $('#exam_name').children('option[value="' + id + '"]')[0].innerHTML;
			if (id == "x") {
				swal("Error", "Silahkan pilih ulangan terlebih dahulu", "error");
			} else {
				window.location.href = "{{ url('/dashboard/nilai/export') }}?id=" + id + "&exam_name=" + exam_name;
			}
		}

		function getNilai() {
			$('#table').DataTable().destroy();
			$('#tableNilai').children().remove();
			$('#load_content').removeClass('d-none');
			if ($('#exam_name').val() == "x") {
				swal('Error!', 'Silahkan pilih ulangan terlebih dahulu.', 'error');
				$('#table').DataTable();
				$('#load_content').addClass('d-none');
			} else {
				$.ajax({
					method: 'GET',
					url: "{{url('/dashboard/nilai/load')}}",
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token]').attr('content')
					},
					data: {
						'id' : $('#exam_name').val()
					}
				}).done(function (data) {
					if(data.value == 1){
						$.each(data.data, function(index, data) {
							var row = $(
								"<tr>" +
									"<td>" + data.nama_depan + ' ' + data.nama_belakang + "</td>" +
									"<td>" + data.kelas + data.posisi + "</td>" +
									"<td>" + data.no_absen + "</td>" +
									"<td>" + data.nilai + "</td>" +
								"</tr>"
							);
							$('#tableNilai').append(row);
						});
					}
					$('#table').DataTable();
					$('#load_content').addClass('d-none');
				});
			}
			
		}
	</script>
@endsection
@extends('dashboard.template')


@section('title', 'Daftar Ulangan')


@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item " href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview is-expanded"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu ">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item active"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item "><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
						Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-laptop"></i> Daftar Ulangan</h1>
				<p>Silahkan membuat rincian ulangan</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">Buat Ulangan Baru</div>
					<div class="tile-body">
						@if ($errors->any())
							<div class="alert alert-dismissible alert-danger">
								<button class="close" type="button" data-dismiss="alert">×</button>
								<strong>Error</strong>
								@foreach ($errors->all() as $error)										
									<p class="mb-0">{{ $error }}</p>
								@endforeach
							</div>
						@else
						@if (session('success'))
							<div class="alert alert-dismissible alert-success">
								<button class="close" type="button" data-dismiss="alert">×</button>
								<strong>Sukses</strong>
								<p class="mb-0">{{ session('success') }}</p>
							</div>
						@endif
						@endif
						<form action=" {{ route('dashboard.ulangan.store') }} " method="post" class="">
						<div class="row">
							@method('POST')
							@csrf
							<div class="form-group col-5">
								<label for="exam_name">Judul Ulangan</label>
								<input type="text" name="exam_name" id="exam_name" class="form-control">
							</div>
							<div class="form-group col-2">
								<label for="exam_time">Waktu (menit)</label>
								<input type="number" name="exam_time" id="exam_time" class="form-control">
							</div>
							<div class="form-group col-2">
								<label for="exam_kkm">KKM</label>
								<input type="number" name="exam_kkm" id="exam_kkm" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-2">
								<label for="exam_class">Kelas</label>
								<select name="exam_class" id="exam_class" class="form-control">
									<option value="7">VII</option>
									<option value="8">VIII</option>
									<option value="9">IX</option>
								</select>
							</div>
							<div class="form-group col-2">
								<label for="exam_status">Status</label>
								<select name="exam_status" id="exam_status" class="form-control">
									<option value="0">Closed</option>
									<option value="1">Open</option>
								</select>
							</div>
							<div class="form-group col-1 align-self-end">
								<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Buat</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">Daftar Ulangan</div>
					<div class="tile-body">
						<div id="error" class="alert alert-danger" style="display: none">
							<button class="close" type="button" onclick="$('.alert-danger').hide();">×</button>
							<strong>Error!</strong>
							<div class="message">
							</div>
						</div>
						<table class="table table-bordered table-striped" id="table">
							<thead class="thead-light">
								<tr>
									<th class="text-center align-middle">Hapus</th>
									<th class="text-center align-middle">Kode</th>
									<th class="text-center align-middle">Judul Ulangan</th>
									<th class="text-center align-middle">Waktu (menit)</th>
									<th class="text-center align-middle">KKM</th>
									<th class="text-center align-middle">Untuk Kelas</th>
									<th class="text-center align-middle">Edit Ulangan</th>
									<th class="text-center align-middle">Ubah Status</th>
									<th class="text-center align-middle">Status Kunci</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($ulangan as $item)
									<tr>
										<td scope="row" class="text-center align-middle">
											<button class="btn btn-sm btn-danger" value="{{ $item->id }}" onclick="hapusUlangan($(this).val())">
												<i class="fa fa-times align-baseline mr-0" aria-hidden="true"></i>
											</button>
										</td>
										<td class="align-middle">
											<span id="exam_id_{{ $item->id }}" class="data_{{ $item->id }}">{{ $item->id }}</span>
										</td>
										<td class="align-middle">
											<span id="exam_name_{{ $item->id }}" class="data_{{ $item->id }}">{{ title_case(str_limit($item->exam_name, 20)) }}</span>
											<input type="text" class="data_{{ $item->id }} form-control d-none" name="exam_name" placeholder="{{ title_case($item->exam_name) }}">
										</td>
										<td class="align-middle">
											<span id="exam_time_{{ $item->id }}" class="data_{{ $item->id }}">{{ $item->exam_time }}</span>
											<input type="text" class="data_{{ $item->id }} form-control d-none" name="exam_time" placeholder="{{ $item->exam_time }}">
										</td>
										<td class="align-middle">
											<span id="exam_kkm_{{ $item->id }}" class="data_{{ $item->id }}">{{ $item->exam_kkm }}</span>
											<input type="text" class="data_{{ $item->id }} form-control d-none" name="exam_kkm" placeholder="{{ $item->exam_kkm }}">
										</td>
										<td class="align-middle">
											<span id="exam_class_{{ $item->id }}" class="data_{{ $item->id }}">
											@if ($item->exam_class == 7)
											VII
											@endif
											@if ($item->exam_class == 8)
											VIII
											@endif
											@if ($item->exam_class == 9)
											IX
											@endif
											</span>
											<select class="data_{{ $item->id }} form-control d-none" name="exam_class" >
												<option value="7">VII</option>
												<option value="8">VIII</option>
												<option value="9">IX</option>
											</select>
										</td>
										<td class="align-middle">
											<button onclick="toggleUpdate({{ $item->id }})" class="edit btn btn-sm btn-info" name="edit_{{ $item->id }}">Edit</button>
											<button onclick="updateUlangan({{ $item->id }})" class="edit d-none btn btn-sm btn-success" name="edit_{{ $item->id }}">Save</button>
											<button onclick="toggleUpdate({{ $item->id }})" class="edit d-none btn btn-sm btn-danger ml-4" name="edit_{{ $item->id }}">Cancel</button>
										</td>
										<td class="align-middle">
											@if ($item->exam_status == 0)
												<button id="toggleButton_{{ $item->id }}" value="{{ $item->id }}" onclick="toggleStatus(this)" class="btn btn-sm btn-danger">Closed</button>
											@endif
											@if ($item->exam_status == 1)
												<button id="toggleButton_{{ $item->id }}" value="{{ $item->id }}" onclick="toggleStatus(this)" class="btn btn-sm btn-success">Open</button>
											@endif
										</td>
										<td class="align-middle">
											@if ($item->exam_shareAnswer == 0)
												<button id="share_{{ $item->id }}" class="btn btn-sm btn-danger" name="{{ $item->id }}" onclick="shareAnswer(this)" value="1">Rahasia</button>
											@endif
											@if ($item->exam_shareAnswer == 1)
												<button id="share_{{ $item->id }}" class="btn btn-sm btn-success" name="{{ $item->id }}" onclick="shareAnswer(this)" value="0">Dibagikan</button>
											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection


@section('jscript')
	<!-- The javascript plugin to display page loading on top-->
	<script src={{ asset("js/plugins/pace.min.js") }}></script>
	<!-- Page specific javascripts-->
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script src={{ asset("js/plugins/sweetalert.min.js") }}></script>
	<script>

		function toggleUpdate(id) {
			$('.data_' + id).toggleClass('d-none');
			$('#toggleButton_' + id).toggleClass('d-none');
			$('#share_' + id).toggleClass('d-none');
			$('button[name="edit_' + id +'"]').toggleClass('d-none');
		}

		function updateUlangan(id) {
			var data_ = {
				"id"			: id,
				"exam_name"		: $('.data_' + id + '[name="exam_name"]').val(),
				"exam_time"		: $('.data_' + id + '[name="exam_time"]').val(),
				"exam_kkm" 		: $('.data_' + id + '[name="exam_kkm"]').val(),
				"exam_class"	: $('.data_' + id + '[name="exam_class"]').val()
			};
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/ulangan/update')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: data_
			}).done(function(e) {
				var judul = e.exam_name;
				$('#exam_name_' + id).text(judul.substr(0, 20) + "....");
				$('#exam_time_' + id).text(e.exam_time);
				$('#exam_kkm_' + id).text(e.exam_kkm);
				$('#exam_class_' + id).text(function() {
					if (e['exam_class'] == 7) {
						return 'VII';
					} else if (e['exam_class'] == 8) {
						return 'VIII';
					} else if (e['exam_class'] == 9) {
						return 'IX';
					} 
				});
				toggleUpdate(id);
			}).fail(function(e) {
				$('#error').children('.message').children().remove();
				$.each(e.responseJSON.errors, function(index, data) {
					$('#error').children('.message').append('<p class="mb-0">' + data + '</p>');
					$('#error').show();
					
				});
			});
		}

		function toggleStatus(el) {
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/ulangan/toggle')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					toggle: $(el).text(),
					id: $(el).val()
				}
			}).done(function(e) {
				if(e['success'] == 1) {
					var text = $("#toggleButton_" + $(el).val()).text();
					$("#toggleButton_" + $(el).val()).text(text == "Open" ? "Closed" : "Open");
					$('#toggleButton_'+ $(el).val()).toggleClass('btn-success').toggleClass('btn-danger');
				}
			});
		}

		function shareAnswer(el) {
			$.ajax({
				method: "POST",
				url: "{{url('/dashboard/ulangan/share')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					toggle: $(el).val(),
					exam_id: $(el).attr('name')
				}
			}).done(function(e) {
				if(e.value == 1) {
					if(e.data.shareAnswer == 0) {
						$("button[name='" + e.data.exam_id + "']").addClass('btn-danger').removeClass('btn-success');
						$("button[name='" + e.data.exam_id + "']").text("Rahasia");
						$("button[name='" + e.data.exam_id + "']").val("1");
					} else if(e.data.shareAnswer == 1) {
						$("button[name='" + e.data.exam_id + "']").addClass('btn-success').removeClass('btn-danger');
						$("button[name='" + e.data.exam_id + "']").text("Dibagikan");
						$("button[name='" + e.data.exam_id + "']").val("0");
					}
				}
			});
		}

		function hapusUlangan (el) {
			var val = $('#exam_name_'+el)[0].innerHTML;
			swal({
				title: 'Anda Yakin Akan Menghapus Ulangan ' + val + ' ?',
				text: 'Ulangan yang telah dihapus tidak dapat dikembalikan. Menghapus ulangan otomatis menghapus seluruh soal.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yakin',
				cancelButtonText: 'Kembali',
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						method: 'DELETE',
						url: "{{url('/dashboard/ulangan/delete')}}",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						data: {
							'id': el
						}
					}).done(function () {
						swal('Success', 'Ulangan ' + val + ' dan soal berhasil dihapus.', 'success');
						window.location.href = "{{ url('/dashboard/ulangan') }}"
					});
				} else {
					swal('Batal', 'Ulangan ' + val + ' tidak diubah.', 'error');
				}
			});
		}

		$(document).ready(function() {
			$('#table').DataTable({
				"scrollX" : true,
				"order" : [
					[1, 'asc'],
				],
				"columnDefs": [
					{ "width": "40px", "orderable": false, "targets": 0},
					{ "width": "60px", "targets": 1},
					{ "width": "240px", "targets": 2},
					{ "width": "60px", "targets": 3},
					{ "width": "40px", "targets": 4},
					{ "width": "80px", "targets": 5},
					{ "width": "160px", "orderable": false, "targets": 6},
					{ "width": "80px", "orderable": false, "targets": 7},
					{ "width": "80px", "orderable": false, "targets": 8},
				]
			});
		})
	</script>
@endsection
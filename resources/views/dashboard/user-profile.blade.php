@extends('dashboard.template')


@section('title', 'User Profil')


@section('konten')
		<ul class="app-menu">
			<li><a href="{{ route('dashboard.index') }}" class="app-menu__item"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li><a href="{{ route('dashboard.ulangan') }}" class="app-menu__item"><i class="app-menu__icon fa fa-external-link"></i><span class="app-menu__label">Ikuti Ulangan</span></a></li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="row user">
			<div class="col-md-12">
				<div class="profile">
					<div class="info">
						<img class="user-img" src="{{ $photo_profile }}">
						<h4>{{ $firstName }} {{ $lastName }}</h4>
						<p>{{ $kelas }} - {{ $posisi }}</p>
					</div>
					<div class="cover-image" style="background-image: url({{ asset($photo_cover) }})">
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="tab-content">
					<div class="tab-pane active" id="user-settings">
						<div class="tile user-settings">
							<h4 class="line-head">Ubah Profile</h4>
							@if ($errors->any())
								<div class="alert alert-dismissible alert-danger">
									<button class="close" type="button" data-dismiss="alert">×</button>
									<strong>Error</strong>
									@foreach ($errors->all() as $error)										
										<p class="mb-0">{{ $error }}</p>
									@endforeach
								</div>
							@endif
							<form action=" {{ route('profile.update') }} " method="post" enctype="multipart/form-data">
								@method('PUT')
								@csrf
								<div class="row mb-4">
									<div class="col-md-4">
										<label>First Name</label>
										<input class="form-control" type="text" name="name_first" placeholder=" {{ $firstName }} ">
									</div>
									<div class="col-md-4">
										<label>Last Name</label>
										<input class="form-control" type="text" name="name_last" placeholder=" {{ $lastName }} ">
									</div>
								</div>
								<div class="row">
									<div class="col-md-8 mb-4">
										<label>Username</label>
										<input class="form-control" type="text" name="username" readonly="readonly" value=" {{ $username }} ">
									</div>
								</div>
								<div class="row">
									<div class="col-md-2 mb-4">
										<label for="kelas">Kelas</label>
										<select name="kelas" id="kelas" class="form-control">
											@if ($kelas == 7)												
												<option value="7" selected>VII</option>
												<option value="8">VIII</option>
												<option value="9">IX</option>
											@endif
											@if ($kelas == 8)												
												<option value="7">VII</option>
												<option value="8" selected>VIII</option>
												<option value="9">IX</option>
											@endif
											@if ($kelas == 9)												
												<option value="7">VII</option>
												<option value="8">VIII</option>
												<option value="9" slected>IX</option>
											@endif
											@if (!isset($kelas))												
												<option value="7">VII</option>
												<option value="8">VIII</option>
												<option value="9">IX</option>
											@endif
										</select>
									</div>
									<div class="col-md-1 mb-4 align-self-end">
										<input type="text" name="posisi" id="posisi" class="form-control" placeholder=" {{ $posisi }} ">
									</div>
									<div class="col-md-2 mb-4">
										<label for="no_absen">Absen</label>
										<input type="number" name="no_absen" id="no_absen" class="form-control" placeholder=" {{ $no_absen }} ">
									</div>
									<div class="col-3 mb-4">
										<label>NIS</label>
										<input class="form-control" type="number" name="nis" id="nis" placeholder=" {{ $nis }} ">
									</div>
								</div>
								<div class="row">
									<div class="col-4 mb-4 align-self-end">
										<label>NISN</label>
										<input class="form-control" type="number" name="nisn" id="nisn" placeholder=" {{ $nisn }} ">
									</div>
								</div>
								<br><br>
								<h4 class="line-head">Ubah Password</h4>
								<div class="row">
									<div class="col-4 mb-4">
										<input class="form-control" type="password" name="password" id="password">
										<small class="form-text text-muted">New Password</small>
									</div>
									<div class="col-4 mb-4 align-self-end">
										<input type="password" class="form-control" name="re_password" id="re_password">
										<small class="form-text text-muted">Confirm Password</small>
									</div>
								</div>
								<div class="row">
									<div class="o col-4 mb-4">
										<label>Ubah Foto Sampul</label>
										<input class="form-control-file" type="file" name="photo_cover">
									</div>
									<div class="col-4 mb-4">
										<label>Ubah Foto Profil</label>
										<input class="form-control-file" type="file" name="photo_profile">
										<small class="form-text text-muted">Lebih bagus apabila rasio foto 1:1</small>
									</div>
								</div>
								<div class="row mb-10">
									<div class="col-md-12">
										<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
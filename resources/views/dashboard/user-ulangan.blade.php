@extends('dashboard.template')


@section('title', 'Daftar Ulangan')


@section('konten')
		<ul class="app-menu">
				<li><a href="{{ route('dashboard.index') }}" class="app-menu__item "><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
				<li><a href="{{ route('dashboard.ulangan') }}" class="app-menu__item active"><i class="app-menu__icon fa fa-external-link"></i><span class="app-menu__label">Ikuti Ulangan</span></a></li>
				<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek Nilai</span></a></li>
			</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-external-link"></i> Ikuti Ulangan</h1>
				<p>Pilih ulangan yang akan diikuti</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					@if (session('error'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong>Error!</strong> 
						<p class="mb-1">{{ session('error') }}</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endif
					<div class="tile-title">Daftar Ulangan</div>
					<div class="tile-body">
						
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="align-middle" style="width: 40%">Judul Ulangan</th>
									<th class="align-middle" style="width: 20%">Waktu Pengerjaan</th>
									<th class="align-middle" style="width: 10%">KKM</th>
									<th class="align-middle" style="width: 10%">Status</th>
									<th class="align-middle" style="width: 20%">Ikuti Ulangan</th>
								</tr>
							</thead>
							<tbody>
									@foreach ($ulangan as $item)
									<tr>
										<td scope="row">{{ title_case(str_limit($item->exam_name, 20)) }}</td>
										<td scope="row">{{ $item->exam_time }} Menit</td>
										<td>{{ $item->exam_kkm }}</td>
										<td>
											@if ($item->exam_status == 0)
												Closed
											@endif
											@if ($item->exam_status == 1)
												Open
											@endif
										</td>
										<td>
											<a href="{{ url('/ulangan/try/'.$item->id) }}" class="btn btn-sm btn-primary">Ikuti Ulangan</a>
										</td>
										
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
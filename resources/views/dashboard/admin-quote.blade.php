@extends('dashboard.template')


@section('title', 'Daftar Quote')

@section('modal')
	{{-- Modal Overlay --}}
	<div class="modal fade" id="quote_detail-overlay" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="quote_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalCenterTitle">Detail & Edit Quote</h5>
			<button type="button" class="close closeButton" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="quote_id-edit" name="quote_id-edit" value="" readonly>
				<div class="row">
					<div class="form-group col-12">
						<label for="quote_content-edit">Quote:</label>
						<textarea name="quote_content-edit" id="quote_content-edit" cols="30" rows="3" class="form-control"></textarea>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group row">
						<label class="control-label col-2 align-self-end" for="quote_by-edit">Quote By:</label>
						<div class="col-6">
							<input type="text" id="quote_by-edit" class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 form-group">
						<button type="button" id="hapusQuote" class="btn btn-danger">Hapus Quote</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			<button type="button" class="closeButton btn btn-secondary">Close</button>
			<button type="button" class="btn btn-primary" onclick="updateQuote()">Save changes</button>
			</div>
		</div>
		</div>
	</div>
@endsection

@section('konten')
		<ul class="app-menu">
			<li><a class="app-menu__item " href="{{ route('dashboard.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
			<li class="treeview is-expanded"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span
					 class="app-menu__label">Kelola Ulangan</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ route('dashboard.ulangan') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Daftar Ulangan</a></li>
					<li><a href="{{ route('dashboard.ulangan.monitor') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i>Monitor Ulangan</a></li>
					<li><a href="{{ route('dashboard.soal') }}" class="treeview-item"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Soal</a></li>
					<li><a href="{{ route('dashboard.quote') }}" class="treeview-item active"><i class="icon fa fa-circle-o" aria-hidden="true"></i> Daftar Quote</a></li>
				</ul>
			</li>
			<li><a href="{{ route('dashboard.nilai') }}" class="app-menu__item"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Cek
						Nilai</span></a></li>
		</ul>
	</aside>
	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-laptop"></i> Daftar Quote</h1>
				<p>Silahkan mengelola quote anda.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="tile">
					<div class="tile-title">
						<span class="mr-4">Tambah Quote</span>
					</div>
					<div class="tile-body" id="quote_body">
						<div class="alert alert-danger" style="display: none">
							<button class="close" type="button" onclick="$('.alert-danger').hide();">×</button>
							<strong>Error!</strong>
							<div class="message mb-2">
							</div>
						</div>
						<div class="alert alert-success" style="display: none">
							<button class="close" type="button" onclick="$('.alert-success').hide();">×</button>
							<strong>Sukses</strong>
							<div class="message mb-2">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label for="quote_content"><strong class="mb-0">Quote Content:</strong> </label>
								<textarea name="quote_content" id="quote_content" cols="30" rows="3" class="form-control" tabindex="1"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-6 form-group">
								<label for="quote_by"><strong class="mb-0 mt-2">Quote By:</strong></label>
								<input type="text" name="quote_by" id="quote_by" class="form-control" tabindex="2">
							</div>
							<div class="col-6 form-group align-self-end">
								<button type="button" onclick="tambahQuote()" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Quote</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-title">
						Daftar Quote
						<a href="javascript:void(0)" onclick="getQuote()" class="ml-4">
							<i class="fa fa-refresh" aria-hidden="true"></i>
						</a>
					</div>
					<div class="tile-body">
						<div class="overlay d-none" id="load_content">
							<div class="m-loader mr-4">
								<svg class="m-circular" viewBox="25 25 50 50">
									<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
								</svg>
							</div>
							<h3 class="l-text">Loading</h3>
						</div>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Quote</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody id="daftar">
		
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection


@section('jscript')
	<!-- The javascript plugin to display page loading on top-->
	<script src={{ asset("js/plugins/pace.min.js") }}></script>
	<!-- Page specific javascripts-->
	<script src={{ asset("js/plugins/jquery.dataTables.min.js") }}></script>
	<script src={{ asset("js/plugins/dataTables.bootstrap.min.js") }}></script>
	<script src={{ asset('js/plugins/sweetalert.min.js') }}></script>
	<script>
		var propp = {
			"columnDefs": [
				{"width": "10%", "targets": 0},
				{"width": "65%", "targets": 1},
				{"width": "25%", "orderable": false,"targets": 2},
			],
			"order": [
				[0, 'asc'],
			],
		}

		function getQuote() {
			$('#table').DataTable().destroy();
			$('#daftar').children().remove();
			$('#load_content').removeClass('d-none');

			$.ajax({
				method: "GET",
				url: "{{url('dashboard/quote/get')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'mode': 0,
				}
			}).done(function(e) {
				$.each(e.data, function(index, data) {
					$('#daftar').append(
						'<tr>' +
							'<td>' + (index+1) + '</td>' +
							'<td>' + data.quote_content + ' - ' + data.quote_by + '</td>' +
							'<td>' +
								'<button class="btn btn-sm btn-primary" value="' + data.quote_id + '" onclick="editQuote(this)">View</button>' +
							'</td>' +
						'</tr>'
					);
				});
				$('#table').DataTable(propp);
				$('#load_content').addClass('d-none');
				$('.fa-refresh').removeClass('fa-spin');
			});
		}

		function tambahQuote() {
			$.ajax({
				method: "POST",
				url: "{{url('dashboard/quote')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'quote_content'	: $('#quote_content').val(),
					'quote_by'		: $('#quote_by').val(),
				}
			}).done(function(e) {
				$('#quote_content').val("");
				$('#quote_by').val("");
				switch (e.value) {
					case 0:
						$('.alert-danger').children('.message').children().remove();
						$.each(e.errors, function(key, val) {
							$('.alert-danger').children('.message').append('<p class="mb-0">' + val + '</p>');
						});
						$('.alert-danger').show();	
						break;
				
					case 1:
						getQuote();
						$('.alert-success').children('.message').children().remove();
						$('.alert-success').children('.message').append('<p class="mb-0">Quote berhasil dibuat.</p>');
						$('.alert-success').show();
						break;
				}
			});
		}

		function editQuote(el) {
			$('#quote_detail-overlay').modal({backdrop: 'static', keyboard: false});
			$('#quote_detail-overlay').modal('show');

			$.ajax({
				method: "GET",
				url: "{{url('dashboard/quote/get')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'mode': 1,
					'quote_id': $(el).val(),
				}
			}).done(function(e) {
				if (e.value == 1) {
					$('#quote_id-edit').val(e.data.quote_id);
					$('#quote_content-edit').val(e.data.quote_content);
					$('#quote_by-edit').val(e.data.quote_by);
					$('#quote_detail').modal('show');
				} else {
					swal('Error', 'Data yang anda cari tidak dapat ditemukan. Silahkan refresh terlebih dahulu.', 'error');
				}
			});
		}

		function updateQuote() {
			$.ajax({
				method: 'PUT',
				url: "{{ url('dashboard/quote') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'quote_id' : $('#quote_id-edit').val(),
					'quote_content' : $('#quote_content-edit').val(),
					'quote_by' : $('#quote_by-edit').val(),
				}
			}).done(function(e) {
				if (e.value == 1) {
					getQuote();
					swal("Sukses", "Quote berhasil diupdate", "success");
				} else {
					swal("Error", "Terjadi kesalahan saat menyimpan quote. Silahkan refresh browser, dan lakukan penyimpanan ulang.", "error");
				}
				$('#quote_detail-overlay').modal('hide');
				$('#quote_detail').modal('hide');
			});
		}

		function hapusQuote(el) {
			$.ajax({
				method: "DELETE",
				url: "{{url('dashboard/quote')}}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'quote_id' : el,
				}
			}).done(function (e) {
				if (e.value == 1) {
					swal("Sukses", "Quote berhasil dihapus", "success");
					getQuote();
				} else {
					swal("Error", "Terjadi kesalahan saat menghapus quote. Silahkan refresh browser, dan lakukan penghapusan ulang.", "error");
				}
				$('#quote_detail-overlay').modal('hide');
				$('#quote_detail').modal('hide');
			});
		}

		$(document).ready(function() {
			$('#table').DataTable(propp);
		});
		
		$('.closeButton').click(function() {
			$('#quote_detail-overlay').modal('hide');
			$('#quote_detail').modal('hide');
		});
		
		$('#hapusQuote').click(function(){
			swal({
				title: "Yakin akan menghapus quote ini?",
				text: "Setelah dihapus, maka quote tidak dapat dikembalikan!",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: "Yakin",
				cancelButtonText: "Batal",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {
					hapusQuote($('#quote_id-edit').val());
				} else {
					swal("Batal", "Quote batal dihapus, dan tidak berubah", "error");
					$('#quote_detail-overlay').modal('hide');
					$('#quote_detail').modal('hide');
				}
			});
		});

		$('.fa-refresh').click(function() {
			$(this).addClass('fa-spin');
		});
	</script>
	@endsection
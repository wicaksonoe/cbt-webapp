@extends('ulangan.template')

@section('title', 'Selesai')

@section('css')
	<style>
		
	</style>
@endsection

@section('body')

	<div class="container" >
		<div class="row align-items-center" style="height: 100vh">
			<div class="col-12 text-center">
				<h1 style="color: rgb(150, 150, 150)">Selamat !</h1>
				<h3 style="color: rgb(150, 150, 150)">Anda Telah Menyelesaikan Ulangan {{ title_case($exam_name) }}</h3>
				<br>
				<div class="row">
					<div class="col-6 offset-3 text-center">
						<h5 style="color: rgb(150, 150, 150)">{{ title_case($quote_content) }}</h5>
						<p style="color: rgb(150, 150, 150)">--{{ title_case($quote_by) }}--</p>
					</div>
				</div>
				<br>
				<h3 style="color: rgb(150, 150, 150)">Perolehan Nilai</h3>
				<h3><strong>{{ $nilai }}</strong></h3>
				<br>
				<br>
				<a href="{{ url('/dashboard') }}">kembali ke dashboard</a>
			</div>
		</div>
    </div>

@endsection

@section('script')
	
	<script>
		
	</script>

@endsection
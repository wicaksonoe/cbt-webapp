@extends('ulangan.template')

@section('title', 'Soal Ulangan')

@section('css')
	<style>
		.mark::after {
			content: "";
			display: block;
			width: 20px;
			border-bottom: 8px solid #ffc107;
		}

		span.nomor {
			width: 48px !important;
			height: 48px !important;
		}
	</style>
@endsection

@section('body')

	<nav class="navbar navbar-light bg-light sticky-top">
		<div class="col-6">
			<p class="text-center mt-2 mb-0">Waktu Anda</p>
			<h2 class="text-center m-0">
				<strong id="current_time">00:00</strong>
			</h2>
		</div>
		<div class="col-6">
			<p class="text-center mt-2 mb-0">Sisa Waktu</p>
			<h2 class="text-center m-0">
				<strong id="remain_time">00:00</strong>
			</h2>
		</div>
	</nav>
	<!-- Akhir Timer -->

	<div class="container-fluid p-4">
		<!-- Identitas -->
		<div class="row">
			<div class="col">
				<table class="table">
					<tbody>
						<tr>
							<td scope="row" style="width: 15%">Nama</td>
							<td style="width: 5px">:</td>
							<td>{{ $user['nama'] }}</td>
						</tr>
						<tr>
							<td scope="row" style="width: 15%">Kelas / No. Absen</td>
							<td style="width: 5px">:</td>
							<td>{{ $user['kelas'].' / '.$user['noAbsen']}}</td>
						</tr>
						<tr>
							<td scope="row" style="width: 15%">Ulangan / Nilai</td>
							<td style="width: 5px">:</td>
							<td>{{ title_case($user['ulangan']).' / '.$user['nilai'] }}</td>
						</tr>
					</tbody>
				</table>
				<a href="{{ route('dashboard.nilai') }}"><<< Kembali ke halaman nilai.</a>
				<br><br>
			</div>
		</div>
		<!-- Akhir Identitas -->
		<div class="row">
			<!-- Soal -->
			<div class="col-9">
				<div class="card">
					<div class="card-body">
						<h5>
							<strong>Silahkan pilih jawaban yang menurut anda paling benar!</strong>
						</h5>
						<div class="wrapp" id="wrapper">
							<div class="soal">
							@foreach ($datas as $i => $data)
								<h3 id="question">{{ ($i+1).'. '.$data->question }}</h3>
								<div class="form-check">
									@if ($data->jawab == 'A' && $data->status == 1)
										<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerA_{{$i}}" value="A" checked>
										<label class="form-check-label bg-success" for="answerA_{{$i}}" id="answerA_caption{{$i}}">{{ $data->opt_a }}</label>
									@else
										@if ($data->jawab == 'A' && $data->status == 0)
											<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerA_{{$i}}" value="A" checked>
											<label class="form-check-label bg-danger" for="answerA_{{$i}}" id="answerA_caption{{$i}}">{{ $data->opt_a }}</label>
										@else
											@if ($data->kunci == 'A')
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerA_{{$i}}" value="A">
												<label class="form-check-label bg-info" for="answerA_{{$i}}" id="answerA_caption{{$i}}">{{ $data->opt_a }}</label>
											@else
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerA_{{$i}}" value="A">
												<label class="form-check-label" for="answerA_{{$i}}" id="answerA_caption{{$i}}">{{ $data->opt_a }}</label>
											@endif
										@endif
									@endif
								</div>
								<div class="form-check">
									@if ($data->jawab == 'B' && $data->status == 1)
										<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerB_{{$i}}" value="B" checked>
										<label class="form-check-label bg-success" for="answerB_{{$i}}" id="answerB_caption{{$i}}">{{ $data->opt_b }}</label>
									@else
										@if ($data->jawab == 'B' && $data->status == 0)
											<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerB_{{$i}}" value="B" checked>
											<label class="form-check-label bg-danger" for="answerB_{{$i}}" id="answerB_caption{{$i}}">{{ $data->opt_b }}</label>
										@else
											@if ($data->kunci == 'B')
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerB_{{$i}}" value="B">
												<label class="form-check-label bg-info" for="answerB_{{$i}}" id="answerB_caption{{$i}}">{{ $data->opt_b }}</label>
											@else
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerB_{{$i}}" value="B">
												<label class="form-check-label" for="answerB_{{$i}}" id="answerB_caption{{$i}}">{{ $data->opt_b }}</label>
											@endif
										@endif
									@endif
								</div>
								<div class="form-check">
									@if ($data->jawab == 'C' && $data->status == 1)
										<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerC_{{$i}}" value="C" checked>
										<label class="form-check-label bg-success" for="answerC_{{$i}}" id="answerC_caption{{$i}}">{{ $data->opt_c }}</label>
									@else
										@if ($data->jawab == 'C' && $data->status == 0)
											<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerC_{{$i}}" value="C" checked>
											<label class="form-check-label bg-danger" for="answerC_{{$i}}" id="answerC_caption{{$i}}">{{ $data->opt_c }}</label>
										@else
											@if ($data->kunci == 'C')
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerC_{{$i}}" value="C">
												<label class="form-check-label bg-info" for="answerC_{{$i}}" id="answerC_caption{{$i}}">{{ $data->opt_c }}</label>
											@else
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerC_{{$i}}" value="C">
												<label class="form-check-label" for="answerC_{{$i}}" id="answerC_caption{{$i}}">{{ $data->opt_c }}</label>
											@endif
										@endif
									@endif
								</div>
								<div class="form-check">
									@if ($data->jawab == 'D' && $data->status == 1)
										<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerD_{{$i}}" value="D" checked>
										<label class="form-check-label bg-success" for="answerD_{{$i}}" id="answerD_caption{{$i}}">{{ $data->opt_d }}</label>
									@else
										@if ($data->jawab == 'D' && $data->status == 0)
											<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerD_{{$i}}" value="D" checked>
											<label class="form-check-label bg-danger" for="answerD_{{$i}}" id="answerD_caption{{$i}}">{{ $data->opt_d }}</label>
										@else
											@if ($data->kunci == 'D')
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerD_{{$i}}" value="D">
												<label class="form-check-label bg-info" for="answerD_{{$i}}" id="answerD_caption{{$i}}">{{ $data->opt_d }}</label>
											@else
												<input class="form-check-input" type="radio" name="answer{{$i}}" id="answerD_{{$i}}" value="D">
												<label class="form-check-label" for="answerD_{{$i}}" id="answerD_caption{{$i}}">{{ $data->opt_d }}</label>
											@endif
										@endif
									@endif
								</div>
								<br><br>
							@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Akhir Soal -->
		</div>
	</div>

@endsection

@section('script')
	<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>
	{{-- <script>
		var nomorSoal = 1;
		var nomorMaks = {{ count($questions) }};
		var waktuAwal = '{{ $time['init'] }}';	// variabel waktu ujian dalam detik

		function zeroFill(str, max) {
			str = str.toString();
			return str.length < max ? zeroFill("0" + str, max) : str;
		}

		function formatWaktu(val) {
			var menit = Math.floor(val / 60);
			var detik = val % 60;
			return zeroFill(menit, 2) + ":" + zeroFill(detik, 2);
		}

		function getSoal(now, next) {
			$.ajax({
				method: 'PUT',
				url: "{{url('/ulangan/try')}}/" + $('#ulanganId').val(),
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'exam_id' : $('#ulanganId').val(),
					'question_id' : $('#idSoal_'+now).val(),
					'user_id' : $('#student_id').val(),
					'answer' : $('input[name="answer"]:checked').val(),
					'marker' : $('span[name=' + now + ']').hasClass('mark') ? 1 : 0,
					'nextQuestion' : $('#idSoal_'+next).val()
				}
			}).done(function(e) {
				if (e.value == 1) {
					$('#question')[0].innerHTML = nomorSoal + ". " + e.data.question;
					$('#answerA_caption')[0].innerHTML = e.data.opt_a;
					$('#answerB_caption')[0].innerHTML = e.data.opt_b;
					$('#answerC_caption')[0].innerHTML = e.data.opt_c;
					$('#answerD_caption')[0].innerHTML = e.data.opt_d;
					if(e.data.marker == 1) {
						if ( !($('span[name=' + next + ']').hasClass('mark')) ) {
							$('span[name=' + next + ']').addClass('mark');
						}
					}

					if(e.data.answer) {
						$('#answer' + e.data.answer).prop('checked', true);
					} else {
						$('#answerX').prop('checked', true)
					}
				} else {
					window.location.href = "{{ url('/ulangan/done') }}/" + e.data.exam_id;
				}
			});
		}

		function postSoal(now) {
			$.ajax({
				method: 'PUT',
				url: "{{url('/ulangan/try')}}/" + $('#ulanganId').val(),
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'exam_id' : $('#ulanganId').val(),
					'question_id' : $('#idSoal_'+now).val(),
					'user_id' : $('#student_id').val(),
					'answer' : $('input[name="answer"]:checked').val(),
					'marker' : $('span[name=' + now + ']').hasClass('mark') ? 1 : 0,
				}
			}).done(function(e) {
				if (e.value != 1) {
					window.location.href = "{{ url('/ulangan/done') }}/" + e.data.exam_id;
				}
			});
		}

		$(document).ready(function () {
			var waktuSekarang = '{{ $time['elapsed'] }}';
			var sisaWaktu = waktuAwal;
			var mulaiWaktu = setInterval(hitungMundur, 1000);

			function hitungMundur() {
				waktuSekarang++;
				$('#remain_time').html(formatWaktu(sisaWaktu - waktuSekarang));
				$('#current_time').html(formatWaktu(waktuSekarang));

				if (waktuSekarang == sisaWaktu) {
					clearInterval(mulaiWaktu);
					$('#submit').click();
				}

			}
			
			// Nav Klik Soal
			$('span.nomor').click(function () {
				getSoal(nomorSoal, $(this).attr('name'));
				nomorSoal = parseInt($(this).attr('name'));
			});

			// Nav to NEXT Soal
			$('#nextSoal').click(function () {
				if(nomorSoal != nomorMaks) {
					getSoal(nomorSoal, nomorSoal += 1);
				}
			});

			// Nav to PREVIOUS Soal
			$('#prevSoal').click(function () {
				if(nomorSoal != 1) {
					getSoal(nomorSoal, nomorSoal -= 1);
				}
			});

			// Mark Soal
			$('span#mark').click(function () {
				$('span[name=' + nomorSoal + ']').toggleClass('mark');
				postSoal(nomorSoal);
			});

			// Submit ulangan
			$('#submit').click(function () {
				swal({
					title: 'Anda Yakin Sudah Selesai ?',
					text: 'Akan lebih baik jika mengecek kembali jawaban anda apabila masih ada waktu yang tersisa. Setelah selesai, anda tidak dapat mengubah jawaban.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yakin',
					cancelButtonText: 'Kembali',
					closeOnConfirm: false,
					closeOnCancel: true
				}, function(isConfirm) {
					if (isConfirm) {
						window.location.href = "{{ url('ulangan/done') }}/" + $('#ulanganId').val();
					}
				});
			});
			
			// on every change of answer
			$('input[type="radio"]').change(function () {
				if ($(this).val() != "X") {
					$('span[name=' + nomorSoal + ']').removeClass('btn-danger').addClass('btn-primary');
				}
				postSoal(nomorSoal);
			});
		})
	</script> --}}

@endsection
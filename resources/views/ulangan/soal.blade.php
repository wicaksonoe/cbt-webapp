@extends('ulangan.template')

@section('title', 'Soal Ulangan')

@section('css')
	<style>
		.mark::after {
			content: "";
			display: block;
			width: 20px;
			border-bottom: 8px solid #ffc107;
		}

		span.nomor {
			width: 48px !important;
			height: 48px !important;
		}
	</style>
@endsection

@section('body')

	<nav class="navbar navbar-light bg-light sticky-top">
		<div class="col-6">
			<p class="text-center mt-2 mb-0">Waktu Anda</p>
			<h2 class="text-center m-0">
				<strong id="current_time">00:00</strong>
			</h2>
		</div>
		<div class="col-6">
			<p class="text-center mt-2 mb-0">Sisa Waktu</p>
			<h2 class="text-center m-0">
				<strong id="remain_time">00:00</strong>
			</h2>
		</div>
	</nav>
	<!-- Akhir Timer -->

	<div class="container-fluid p-4">
		<!-- Identitas -->
		<div class="row">
			<div class="col">
				<table class="table">
					<tbody>
						<tr>
							<td scope="row" style="width: 15%">Nama</td>
							<td style="width: 5px">:</td>
							<td>{{ $user['nama'] }}</td>
						</tr>
						<tr>
							<td scope="row" style="width: 15%">Kelas / No. Absen</td>
							<td style="width: 5px">:</td>
							<td>{{ $user['kelas'].' / '.$user['noAbsen']}}</td>
						</tr>
						<tr>
							<td scope="row" style="width: 15%">Ulangan</td>
							<td style="width: 5px">:</td>
							<td>
								<input type="text" id="ulanganId" value="{{ $ulangan['id'] }}" readonly hidden>
								{{ title_case($ulangan['name']) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- Akhir Identitas -->
		@foreach ($questions as $key => $item)
			<input type="text" style="width:20px" value="{{ $item->id }}" id="idSoal_{{ $key + 1 }}"  readonly hidden>
		@endforeach
		<div class="row">
			<!-- Soal -->
			<div class="col-9">
				<div class="card">
					<div class="card-body">
						<h5>
							<strong>Silahkan pilih jawaban yang menurut anda paling benar!</strong>
						</h5>
						<input type="text" name="student_id" id="student_id" value="{{ $user['id'] }}"  readonly hidden>
						<div class="wrapp" id="wrapper">
							<div class="soal">
								<h3 id="question">1. {{ $questions[0]->question }}</h3>
								<div class="form-check">
									@if ($questions[0]->answer == 'A')
										<input class="form-check-input" type="radio" name="answer" id="answerA" value="A" checked>
									@else
										<input class="form-check-input" type="radio" name="answer" id="answerA" value="A">
									@endif
									<label class="form-check-label" for="answerA" id="answerA_caption">{{ $questions[0]->opt_a }}</label>
								</div>
								<div class="form-check">
									@if ($questions[0]->answer == 'B')
										<input class="form-check-input" type="radio" name="answer" id="answerB" value="B" checked>
									@else
										<input class="form-check-input" type="radio" name="answer" id="answerB" value="B">
									@endif
									<label class="form-check-label" for="answerB" id="answerB_caption">{{ $questions[0]->opt_b }}</label>
								</div>
								<div class="form-check">
									@if ($questions[0]->answer == 'C')
										<input class="form-check-input" type="radio" name="answer" id="answerC" value="C" checked>
									@else
										<input class="form-check-input" type="radio" name="answer" id="answerC" value="C">
									@endif
									<label class="form-check-label" for="answerC" id="answerC_caption">{{ $questions[0]->opt_c }}</label>
								</div>
								<div class="form-check">
									@if ($questions[0]->answer == 'D')
										<input class="form-check-input" type="radio" name="answer" id="answerD" value="D" checked>
									@else
										<input class="form-check-input" type="radio" name="answer" id="answerD" value="D">
									@endif
									<label class="form-check-label" for="answerD" id="answerD_caption">{{ $questions[0]->opt_d }}</label>
								</div>
								@if ($questions[0]->answer == 'X')
									<input type="radio" name="answer" id="answerX" value="X"  checked>
								@else
									<input type="radio" name="answer" id="answerX" value="X" >
								@endif
							</div>
							<div class="text-center">
								<span id="prevSoal" class="btn btn-primary">
									<i class="fa fa-arrow-left"></i>
								</span>
								<span id="mark" class="btn btn-primary">TANDAI</span>
								<span id="nextSoal" class="btn btn-primary">
									<i class="fa fa-arrow-right"></i>
								</span>
							</div>
						</div>
						<a href="#" class="btn btn-sm btn-primary float-right" id="submit">SELESAI</a>
					</div>
				</div>
			</div>
			<!-- Akhir Soal -->

			<!-- Nav Soal -->
			<div class="col-3">
				<div class="" style="height: 100%">
					@foreach ($questions as $key => $question)
						@if ($question->answer != 'X')
							@if ($question->marker == 1)
								<span name="{{ $key + 1 }}" class="btn btn-primary m-2 nomor mark">
									<strong>{{ sprintf("%02d", $key + 1) }}</strong>
								</span>
							@else
								<span name="{{ $key + 1 }}" class="btn btn-primary m-2 nomor">
									<strong>{{ sprintf("%02d", $key + 1) }}</strong>
								</span>
							@endif
						@else
							@if ($question->marker == 1)
								<span name="{{ $key + 1 }}" class="btn btn-danger m-2 nomor mark">
									<strong>{{ sprintf("%02d", $key + 1) }}</strong>
								</span>
							@else
								<span name="{{ $key + 1 }}" class="btn btn-danger m-2 nomor">
									<strong>{{ sprintf("%02d", $key + 1) }}</strong>
								</span>
							@endif
						@endif
					@endforeach
				</div>
			</div>
			<!-- Akhir Nav Soal -->
		</div>
	</div>

@endsection

@section('script')
	<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>
	<script>
		var nomorSoal = 1;
		var nomorMaks = {{ count($questions) }};
		var waktuAwal = '{{ $time['init'] }}';	// variabel waktu ujian dalam detik

		function zeroFill(str, max) {
			str = str.toString();
			return str.length < max ? zeroFill("0" + str, max) : str;
		}

		function formatWaktu(val) {
			var menit = Math.floor(val / 60);
			var detik = val % 60;
			return zeroFill(menit, 2) + ":" + zeroFill(detik, 2);
		}

		function getSoal(now, next) {
			$.ajax({
				method: 'PUT',
				url: "{{url('/ulangan/try')}}/" + $('#ulanganId').val(),
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'exam_id' : $('#ulanganId').val(),
					'question_id' : $('#idSoal_'+now).val(),
					'user_id' : $('#student_id').val(),
					'answer' : $('input[name="answer"]:checked').val(),
					'marker' : $('span[name=' + now + ']').hasClass('mark') ? 1 : 0,
					'nextQuestion' : $('#idSoal_'+next).val()
				}
			}).done(function(e) {
				if (e.value == 1) {
					$('#question')[0].innerHTML = nomorSoal + ". " + e.data.question;
					$('#answerA_caption')[0].innerHTML = e.data.opt_a;
					$('#answerB_caption')[0].innerHTML = e.data.opt_b;
					$('#answerC_caption')[0].innerHTML = e.data.opt_c;
					$('#answerD_caption')[0].innerHTML = e.data.opt_d;
					if(e.data.marker == 1) {
						if ( !($('span[name=' + next + ']').hasClass('mark')) ) {
							$('span[name=' + next + ']').addClass('mark');
						}
					}

					if(e.data.answer) {
						$('#answer' + e.data.answer).prop('checked', true);
					} else {
						$('#answerX').prop('checked', true)
					}
				} else {
					window.location.href = "{{ url('/ulangan/done') }}/" + e.data.exam_id;
				}
			});
		}

		function postSoal(now) {
			$.ajax({
				method: 'PUT',
				url: "{{url('/ulangan/try')}}/" + $('#ulanganId').val(),
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					'exam_id' : $('#ulanganId').val(),
					'question_id' : $('#idSoal_'+now).val(),
					'user_id' : $('#student_id').val(),
					'answer' : $('input[name="answer"]:checked').val(),
					'marker' : $('span[name=' + now + ']').hasClass('mark') ? 1 : 0,
				}
			}).done(function(e) {
				if (e.value != 1) {
					window.location.href = "{{ url('/ulangan/done') }}/" + e.data.exam_id;
				}
			});
		}

		$(document).ready(function () {
			var waktuSekarang = '{{ $time['elapsed'] }}';
			var sisaWaktu = waktuAwal;
			var mulaiWaktu = setInterval(hitungMundur, 1000);

			function hitungMundur() {
				waktuSekarang++;
				$('#remain_time').html(formatWaktu(sisaWaktu - waktuSekarang));
				$('#current_time').html(formatWaktu(waktuSekarang));

				if (waktuSekarang == sisaWaktu) {
					clearInterval(mulaiWaktu);
					$('#submit').click();
				}

			}
			
			// Nav Klik Soal
			$('span.nomor').click(function () {
				getSoal(nomorSoal, $(this).attr('name'));
				nomorSoal = parseInt($(this).attr('name'));
			});

			// Nav to NEXT Soal
			$('#nextSoal').click(function () {
				if(nomorSoal != nomorMaks) {
					getSoal(nomorSoal, nomorSoal += 1);
				}
			});

			// Nav to PREVIOUS Soal
			$('#prevSoal').click(function () {
				if(nomorSoal != 1) {
					getSoal(nomorSoal, nomorSoal -= 1);
				}
			});

			// Mark Soal
			$('span#mark').click(function () {
				$('span[name=' + nomorSoal + ']').toggleClass('mark');
				postSoal(nomorSoal);
			});

			// Submit ulangan
			$('#submit').click(function () {
				swal({
					title: 'Anda Yakin Sudah Selesai ?',
					text: 'Akan lebih baik jika mengecek kembali jawaban anda apabila masih ada waktu yang tersisa. Setelah selesai, anda tidak dapat mengubah jawaban.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yakin',
					cancelButtonText: 'Kembali',
					closeOnConfirm: false,
					closeOnCancel: true
				}, function(isConfirm) {
					if (isConfirm) {
						window.location.href = "{{ url('ulangan/done') }}/" + $('#ulanganId').val();
					}
				});
			});
			
			// on every change of answer
			$('input[type="radio"]').change(function () {
				if ($(this).val() != "X") {
					$('span[name=' + nomorSoal + ']').removeClass('btn-danger').addClass('btn-primary');
				}
				postSoal(nomorSoal);
			});
		})
	</script>

@endsection
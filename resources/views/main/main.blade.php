@extends('main.template')

@section('title', 'Aplikasi Ulangan Berbasis Komputer')

@section('konten')
	<section style="width:100vw; height:100vh; position: absolute; z-index: -1;">
		<div class="bg-primary" style="width: 100vw; height: 50vh"></div>
	</section>

	<section style="width:100vw; height:100vh; position: absolute; z-index: 1">
		<div class="container">
			<div class="row align-items-center justify-content-center" style="height: 100vh">
				<div class="bg-white d-flex align-items-center justify-content-center shadow-lg" style="min-height: 500px; width: 350px">
					<div class="col-10">
						<h2 class="text-center"><i class="fa fa-user">&nbsp;</i> Sign In</h2>
						<hr>
						@if ($errors->any())
							<div class="alert alert-dismissible alert-danger">
								<button class="close" type="button" data-dismiss="alert">×</button>
								<strong>Error</strong>
								@foreach ($errors->all() as $error)										
									<p class="mb-0">{{ $error }}</p>
								@endforeach
							</div>
						@else
							@if (session('error'))
								<div class="alert alert-dismissible alert-danger">
									<button class="close" type="button" data-dismiss="alert">×</button>
									<strong>Error</strong>
									<p class="mb-0">{{ session('error') }}</p>
								</div>
							@else
								@if (session('success'))
									<div class="alert alert-dismissible alert-success">
										<button class="close" type="button" data-dismiss="alert">×</button>
										<strong>Sukses</strong>
										<p class="mb-0">{{ session('success') }}</p>
									</div>
								@endif
							@endif
						@endif
						<br>
						<form action="{{ route('main.login') }}" method="POST">
							@method('POST')
							@csrf
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<i class="fa fa-user"></i>
									</div>
								</div>
								<input type="text" name="username" id="username" class="form-control" placeholder="Username">
							</div>
							<br>
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<i class="fa fa-lock"></i>
									</div>
								</div>
								<input type="password" name="password" id="password" class="form-control" placeholder="Password">
							</div>
							<br>
							<button type="submit" class="btn btn-primary btn-sm col">
								<i class="fa fa-sign-in">&nbsp;&nbsp;</i>SIGN IN
							</button>
							<a href="{{ route('register.index') }}" class="btn btn-link text-center col">Buat Akun Baru</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection
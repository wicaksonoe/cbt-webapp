@extends('main.template')

@section('title', 'Buat Akun Baru')

@section('konten')
	<section style="width:100vw; height:100vh; position: absolute; z-index: -1;">
		<div class="bg-primary" style="width: 100vw; height: 50vh"></div>
	</section>

	<section style="width:100vw; height:100vh; position: absolute; z-index: 1">
		<div class="container">
			<div class="row align-items-center justify-content-center" style="height: 100vh">
				<div class="bg-white d-flex align-items-center justify-content-center shadow-lg" style="height: 600px; width: 500px">
					<div class="">
						<h2 class="text-center"><i class="fa fa-pencil">&nbsp;</i> Buat Akun Baru</h2>
						<hr>
						@if ($errors->any())
							<div class="alert alert-dismissible alert-danger">
								<button class="close" type="button" data-dismiss="alert">×</button>
								<strong>Error</strong>
								@foreach ($errors->all() as $error)										
									<p class="mb-0">{{ $error }}</p>
								@endforeach
							</div>
						@endif
						<br>
						<form action=" {{ route('register.store') }} " method="post" id="form_regist">
							@method('POST')
							@csrf
							<div class="row">
								<div class="col-9 mb-4">
									<label>Username</label>
									<input class="form-control" type="text" name="username" id="username">
									<small id="status_result" class="text-success"></small>
								</div>
								<div class="col-1 mb-4 align-self-end">
									<input type="button" value="Cek" class="btn btn-primary" id="check_">
									<small id="status_result_" class="text-white"></small>
								</div>
							</div>
							<div class="row">
								<div class="col-12 mb-4">
									<select name="level" id="level" class="form-control">
										<option value="">-- Saya Seorang --</option>
										<option value="0">Guru</option>
										<option value="1">Siswa/i</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-6 mb-4">
									<label>Password</label>
									<small class="form-text text-danger" id="invalid"></small>
									<input class="form-control" type="password" name="password" id="password">
									<small class="form-text text-muted">New Password</small>
								</div>
								<div class="col-6 mb-4 align-self-end">
									<input type="password" class="form-control" name="re_password" id="re_password">
									<small class="form-text text-muted">Confirm Password</small>
								</div>
							</div>
							<div class="row mb-10">
								<div class="col-md-12">
									<button class="btn btn-primary mr-4" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Buat</button>
									<a href="{{ route('main.index') }}" class="ml-2">Saya sudah punya akun</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('jscript')
	<script>
		$(document).ready(function () {

			$('#form_regist').submit(function (e) {
				e.preventDefault();
				if ( $('#password').val() == $('#re_password').val() && $('#password').val() !== "" && $('#re_password').val() !== "") {
					this.submit()
				} else {
					$('#password').addClass('is-invalid');
					$('#re_password').addClass('is-invalid');
					$('#invalid').html("Password harus diisi dan sama");
				}
			})

			$('#check_').click(function () {
				// AJAX REQUEST HERE
				$.ajax({
					method: "GET",
					url: "{{url('check')}}",
					data: {nama: $('#username').val()}
				}).done(function(e) {
					if (e == 'TRUE') {
						// ON FAILED
						$('#username').addClass("is-invalid");
						$('#username').removeClass("is-valid");
						$('#status_result').addClass("text-danger");
						$('#status_result').removeClass("text-success");
						$('#status_result').html("Username Tidak Dapat Digunakan.");
						$('#status_result_').html("ok");
					} else {
						// ON SUCCESS
						$('#username').addClass("is-valid");
						$('#username').removeClass("is-invalid");
						$('#status_result').removeClass("text-danger");
						$('#status_result').addClass("text-success");
						$('#status_result').html("Username Dapat Digunakan.");
						$('#status_result_').html("ok");
					}
				
				});
			});
		});
	</script>
@endsection
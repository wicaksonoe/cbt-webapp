<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('exam_id');
			$table->text('question')->nullable();
			$table->text('opt_a')->nullable();
			$table->text('opt_b')->nullable();
			$table->text('opt_c')->nullable();
			$table->text('opt_d')->nullable();
			$table->char('opt_ok', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('nama_depan', 255)->nullable();
			$table->string('nama_belakang', 255)->nullable();
			$table->string('kelas', 50)->nullable();
			$table->string('posisi', 30)->nullable();
			$table->integer('no_absen')->nullable();
			$table->integer('nis')->nullable();
			$table->integer('nisn')->nullable();
			$table->string('photo_profile', 255)->default('storage/images/_init-profile.jpg');
			$table->string('photo_cover', 255)->default('storage/images/_init-cover.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}

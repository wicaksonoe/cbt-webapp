<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->text('exam_name');
			$table->integer('exam_time');
			$table->integer('exam_kkm');
			$table->integer('exam_status');
			$table->integer('exam_shareAnswer')->default(0);
			$table->integer('exam_class');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}

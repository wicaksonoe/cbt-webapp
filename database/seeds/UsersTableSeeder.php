<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for ($i=1; $i<=10; $i++){
			$user = str_random(10);
			DB::table('users')->insert([
				'username'	=> $user,
				'password'	=> Hash::make($user),
				'level'		=> rand(0, 1)
			]);
		}
    }
}

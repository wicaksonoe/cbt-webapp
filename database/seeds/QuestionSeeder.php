<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for ($i=0; $i < 10; $i++) { 
			DB::table('questions')->insert([
				'exam_id' => 5,
				'question' => str_random(10),
				'opt_a' => str_random(10),
				'opt_b' => str_random(10),
				'opt_c' => str_random(10),
				'opt_d' => str_random(10),
				'opt_ok' => chr(rand(65, 68))
			]);
		}
    }
}

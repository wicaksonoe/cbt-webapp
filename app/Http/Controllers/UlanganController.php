<?php

namespace App\Http\Controllers;

use Response;
use Image;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Exam;
use App\Report;

class UlanganController extends Controller
{
	// Display Ulangan Dashboard based on level User
	public function index()
	{
		// Load data to pass for view
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$data_ = [
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];
		
		if(isset($data)){
			switch ($data->level) {
				case 0:
					// Get Exam list based from USER_ID
					$ulangan = Exam::where('user_id', $data->id)->get();
					$data_['ulangan'] = $ulangan;

					return view('dashboard.admin-ulangan', $data_);	
					break;
				
				case 1:
					// Get Exam list based from KELAS && STATUS == OPEN
					$ulangans = Exam::where('exam_status', 1)->where('exam_class', $profile->kelas)->get();
					$data_['ulangan'] = [];

					if ( count($ulangans) != 0 ) {
						foreach($ulangans as $i => $ulangan) {
							$data_['ulangan'][$i] = $ulangan;
						}
					}
					
					return view('dashboard.user-ulangan', $data_);	
					break;

				default:
					Auth::logout();
					abort(401);			
					break;
			}
		}
	}

	public function shareUlanganKunci(Request $request)
	{
		$ulangan = Exam::where('id', $request->exam_id)->get();

		if(count($ulangan) == 1) {
			$ulangan[0]->exam_shareAnswer = $request->toggle;
			$ulangan[0]->save();
			
			$return = [
				'value' => 1,
				'status' => 'success',
				'data' => [
					'exam_id' => $request->exam_id,
					'shareAnswer' => $ulangan[0]->exam_shareAnswer,
				],
			];
		} else {
			$return = [
				'value' => 0,
				'status' => 'failed',
				'data' => null
			];
		}
		
		return Response::json($return, 200);
	}

	public function toggleStatus(Request $request)
	{	
		if ($request->ajax()) {
			$return['success'] = 0;
			if ($request['toggle'] == "Open") {
				$data = Exam::find($request['id']);
				$data->exam_status = 0;
				$data->save();
				$return = [
					"id" => $request['id'],
					"stts" => $data->exam_status,
					"success" => 1
				];
			} elseif ($request['toggle'] == "Closed") {
				$data = Exam::find($request['id']);
				$data->exam_status = 1;
				$data->save();
				$return = [
					"id" => $request['id'],
					"stts" => $data->exam_status,
					"success" => 1
				];
			}
			return $return;
		} else {
			abort(401);
		}
	}

	// Store NEW Ulangan based on UserID
	public function ulanganStore(Request $request)
	{
		$rules = [
			'exam_time'		=> 'bail|required|numeric|min:5',
			'exam_class'	=> 'bail|required|numeric|min:7|max:9',
			'exam_kkm'		=> 'bail|required|numeric',
			'exam_status'	=> 'bail|required|boolean',
			'exam_name'		=> 'bail|required|min:4|unique:exams,exam_name',
		];

		$message = [
			'exam_name.required'	=> 'Kolom judul ulangan harus diisi.',
			'exam_name.min'			=> 'Judul ulangan minimal 4 karakter.',
			'exam_name.unique'		=> 'Judul ulangan sudah ada, silahkan buat yang baru.',
			
			'exam_time.required'	=> 'Kolom waktu ulangan harus diisi.',
			'exam_time.numeric'		=> 'Kolom waktu ulangan harus diisi dengan angka.',
			'exam_time.min'			=> 'Waktu ulangan minimal 5 menit.',
			
			'exam_kkm.required'		=> 'Kolom KKM harus diisi.',
			'exam_kkm.numeric'		=> 'Kolom KKM harus diisi dengan angka.',
			
			'exam_status.required'	=> 'Silahkan pilih status ulangan.',
			'exam_status.boolean'	=> 'Silahkan pilih status ulangan.',
			
			'exam_class.required'	=> 'Silahkan pilih kategori kelas.',
			'exam_class.numeric'	=> 'Silahkan pilih kategori kelas.',
			'exam_class.min'		=> 'Silahkan pilih kategori kelas.',
			'exam_class.max'		=> 'Silahkan pilih kategori kelas.',
		];

		Validator::make($request->all(), $rules, $message)->validate();

		$newUlangan = new Exam();

		$newUlangan->user_id		= Auth::user()->id;
		$newUlangan->exam_name		= $request->exam_name;
		$newUlangan->exam_time		= $request->exam_time;
		$newUlangan->exam_kkm		= $request->exam_kkm;
		$newUlangan->exam_status	= $request->exam_status;
		$newUlangan->exam_class		= $request->exam_class;
		$newUlangan->save();
		
		return redirect()->route('dashboard.ulangan')->with('success', 'Ulangan berhasil ditambahkan.');
	}

	// Update existing Ulangan
	public function updateUlangan(Request $request)
	{
		$rules = [
			'exam_kkm'		=> 'bail|nullable|numeric',
			'exam_time'		=> 'bail|nullable|numeric|min:5',
			'exam_class'	=> 'bail|nullable|numeric|min:7|max:9',
			'exam_name'		=> 'bail|nullable|min:4|unique:exams,exam_name',
		];

		$message = [
			'exam_name.min'			=> 'Judul ulangan minimal 4 karakter.',
			'exam_name.unique'		=> 'Judul ulangan sudah ada, silahkan buat yang baru.',
			
			'exam_time.numeric'		=> 'Kolom waktu ulangan harus diisi dengan angka.',
			'exam_time.min'			=> 'Waktu ulangan minimal 5 menit.',
			
			'exam_kkm.numeric'		=> 'Kolom KKM harus diisi dengan angka.',
			
			'exam_class.numeric'	=> 'Silahkan pilih kategori kelas.',
			'exam_class.min'		=> 'Silahkan pilih kategori kelas.',
			'exam_class.max'		=> 'Silahkan pilih kategori kelas.',
		];
		
		if ($request->ajax()) {
			$validate = Validator::make($request->all(), $rules, $message);

			if($validate->fails()) {
				return Response::json([
					'errors' => $validate->getMessageBag()->toArray()
				], 400);
			}

			$data = Exam::find($request['id']);
			(!isset($request->exam_name)	? : $data->exam_name 	= $request->exam_name);
			(!isset($request->exam_time)	? : $data->exam_time 	= $request->exam_time);
			(!isset($request->exam_kkm)		? : $data->exam_kkm 	= $request->exam_kkm);
			(!isset($request->exam_class)	? : $data->exam_class 	= $request->exam_class);
			$data->save();
			
			$return = [
				'exam_name' => $data->exam_name,
				'exam_time' => $data->exam_time,
				'exam_kkm' => $data->exam_kkm,
				'exam_class' => $data->exam_class,
			];
			
			return Response::json($return, 200);
		} else {
			abort(401);
		}
	}

	// DELETE Ulangan
	public function deleteUlangan(Request $request)
	{
		if ($request->ajax()) {
			$data = Exam::where('id', $request->id);
			if ($data->count() == 1) {
				$soal = $data->first()->soal;
				foreach ($soal as $child) {
					$child->delete();
				}
				$data->delete();

				$return = [
					'value' => 1,
					'status' => 'success',
				];
				
				return $return;
			} else {
				$return = [
					'value' => 0,
					'status' => 'failed',
				];
				
				return $return;
			}
		} else {
			abort(401);
		}
	}

	// function untuk monitoring peserta ulangan
	public function ulanganMonitoring()
	{
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$ulangan = Exam::select('id', 'exam_name')->where('user_id', $data->id)->get();

		$data_ = [
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'ulangan'	=> $ulangan,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];

		return view('dashboard.admin-monitor', $data_);
	}

	// function untuk GET peserta ulangan
	public function ulanganMonitoringGet(Request $request)
	{
		$pesertas = Report::where('exam_id', $request->id)->where('status', 1);
		
		if ($pesertas->count() > 0) {
			$return = [
			'value' => 1,
				'status' => 'success'
			];
		} else {
			$return = [
				'value' => 0,
				'status' => 'failed'
			];
		}
		
		foreach ($pesertas->get() as $key => $peserta) {
			$return['data'][$key] = [
				'id' => $peserta->profile->user_id,
				'nama' => title_case($peserta->profile->nama_depan).' '.title_case($peserta->profile->nama_belakang),
				'kelas' => $peserta->profile->kelas.$peserta->profile->posisi,
				'absen' => $peserta->profile->no_absen
			];
		}

		return $return;
	}

	// function untuk FORCE FINISH peserta ulangan
	public function ulanganForceFinish(Request $request)
	{
		$status = Report::where('user_id', $request->user_id)->where('exam_id', $request->exam_id);

		$return = [
			'value' => 0,
			'status' => 'failed',
			'data' => [
				'id' => $request->user_id
			]
		];

		if ($status->count() == 1) {
			$set = $status->first();
			
			$set->status = 0;
			$set->save();
			
			$return = [
				'value' => 1,
				'status' => 'success',
				'data' => [
					'id' => $request->user_id
				]
			];
		}
		
		return $return;
	}
}

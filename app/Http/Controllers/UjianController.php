<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Question;
use App\User;
use App\Report;
use App\Answer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UjianController extends Controller
{
    public function index($id)
	{
		if(!isset($id)) {
			return redirect()->route('dashboard.ulangan')->with('error', 'Silahkan pilih ulangan terlebih dahulu.');
		}

		$user_id = Auth::user()->id;
		$user = User::find($user_id)->profile;
		$ulangan = Exam::find($id);

		$report_status = Report::where('user_id', $user_id)
							->where('exam_id', $id)
							->count();

		if ($report_status == 0) {
			$report = new Report;
			$report->user_id = $user_id;
			$report->exam_id = $id;
			$report->status = 1;
			$report->save();
		}
		

		$time_created = Report::where('user_id', $user_id)
							->where('exam_id', $id)
							->select('created_at')
							->first()->created_at;

		$time_elapsed = time() - strtotime($time_created);
		
		$data['user'] = [
			'nama' => $user->nama_depan.' '.$user->nama_belakang,
			'kelas' => $user->kelas.$user->posisi,
			'noAbsen' => $user->no_absen,
			'id' => $user->user_id
		];
		
		$data['ulangan'] = [
			'id' => $ulangan->id,
			'name' => $ulangan->exam_name
		];

		$data['time'] = [
			'init' => $ulangan->exam_time * 60,
			'elapsed' => $time_elapsed
		];

		$data['questions'] = Question::where('exam_id', $id)
								->select('id', 'question', 'opt_a', 'opt_b', 'opt_c', 'opt_d')
								->inRandomOrder()
								->get();
		
		foreach ($data['questions'] as $key => $value) {
			$relation = $value->jawaban()->select('answer', 'marker')->where('user_id', $user_id)->first();
			
			$data['questions'][$key] = (object) [
				'id' => $value->id,
				'question' => $value->question,
				'opt_a' => $value->opt_a,
				'opt_b' => $value->opt_b,
				'opt_c' => $value->opt_c,
				'opt_d' => $value->opt_d,
				'answer' => isset($relation) ? $relation->answer : 'X',
				'marker' => isset($relation) ? $relation->marker :  0  ,
			];
		}

		return view('ulangan.soal', $data);
	}

	public function getSoal($request)
	{
		$nextQuestion = Question::select('id', 'question', 'opt_a', 'opt_b', 'opt_c', 'opt_d')->where('id', $request->nextQuestion)->first(); 
		$nextAnswer = Answer::select('answer', 'marker')->where('exam_id', $request->exam_id)->where('question_id', $request->nextQuestion)->where('user_id', $request->user_id)->first();

		$return = [
			'question' => $nextQuestion->question,
			'opt_a' => $nextQuestion->opt_a,
			'opt_b'  => $nextQuestion->opt_b,
			'opt_c'  => $nextQuestion->opt_c,
			'opt_d'  => $nextQuestion->opt_d,
			'answer' => isset($nextAnswer->answer) ? $nextAnswer->answer : 'X',
			'marker' => isset($nextAnswer->marker) ? $nextAnswer->marker : 0
		];
		
		return $return;
	}

	public function putAnswer(Request $request)
	{
		// Check every MURID request is still available for them
		$report = Report::where('user_id', $request->user_id)->where('exam_id', $request->exam_id);
		if ($report->count() == 1 && $report->first()->status == 0) {
			$return = [
				'value' => 0,
				'status' => 'failed',
				'data' => [
					'exam_id' => $request->exam_id
				]
			];
			
			return $return;
		}

		// IF MURID still can continue. Continue the request.
		$answer = Answer::where('exam_id', $request->exam_id)->where('question_id', $request->question_id)->where('user_id', $request->user_id);

		if($answer->count() == 0) {
			$newAnswer = new Answer();
			$newAnswer->exam_id = $request->exam_id;
			$newAnswer->question_id = $request->question_id;
			$newAnswer->user_id = $request->user_id;
			$newAnswer->answer = $request->answer;
			$newAnswer->marker = $request->marker;
			$newAnswer->save();
		} else if ($answer->count() == 1) {
			$newAnswer = $answer->first();
			$newAnswer->exam_id = $request->exam_id;
			$newAnswer->question_id = $request->question_id;
			$newAnswer->user_id = $request->user_id;
			$newAnswer->answer = $request->answer;
			$newAnswer->marker = $request->marker;
			$newAnswer->save();
		}

		if ( isset($request->nextQuestion) ) {
			$return = [
				'value' => 1,
				'status' => 'success',
				'data' => $this->getSoal($request)
			];
		} else {
			$return = [
				'value' => 1,
				'status' => 'success',
			];
		}
		
		return $return;
	}

	public function ujianSelesai($id)
	{
		if(!isset($id)) {
			return redirect()->route('dashboard.ulangan')->with('error', 'Silahkan pilih ulangan terlebih dahulu.');
		}

		$quote = DB::table('quotes')->inRandomOrder()->first();
		$user_id = Auth::user()->id;
		$exam_id = $id;
		$points = 0;

		$answers = Answer::where('user_id', $user_id)->where('exam_id', $exam_id)->get();
		foreach ($answers as $key => $answer) {
			if($answer->answer == $answer->kunci->opt_ok) {
				$points++;
			}
		}

		$jumlahSoal = Question::where('exam_id', $exam_id)->count();

		$nilaiAkhir = ($points/$jumlahSoal)*100;

		$report = Report::where('exam_id', $exam_id)->where('user_id', $user_id)->first();
		$report->point = round($nilaiAkhir, 1);
		$report->status = 0;
		$report->save();
		
		$data = [
			'exam_name' => Exam::find($exam_id)->exam_name,
			'quote_content' => $quote->quote_content,
			'quote_by' => $quote->quote_by,
			'nilai' => round($nilaiAkhir, 1)
		];

		return view('ulangan.done', $data);
	}

	public function ujianReview($id)
	{
		if(!isset($id)) {
			return redirect()->route('dashboard.ulangan')->with('error', 'Silahkan pilih ulangan terlebih dahulu.');
		}

		$user_id = Auth::user()->id;
		$user_data = User::find($user_id)->profile;
		$ulangan = Exam::where('id', $id)->first();
		$jawabans = Answer::where('exam_id', $id)->where('user_id', $user_id)->get();
		$nilai = Report::where('user_id', $user_id)->where('exam_id', $id)->first();

		if(count($jawabans) == 0) {
			return redirect()->route('dashboard.ulangan')->with('error', 'Silahkan ikuti ulangan terlebih dahulu.');
		}

		$array['user'] = [
			'nama'		=> $user_data->nama_depan.' '.$user_data->nama_belakang,
			'kelas'		=> $user_data->kelas,
			'noAbsen'	=> $user_data->no_absen,
			'ulangan'	=> $ulangan->exam_name,
			'nilai'		=> $nilai->point,
		];

		if($ulangan->exam_shareAnswer == 1) {
			// return jawaban dan kunci jawaban dan status benar/salah
			foreach ($jawabans as $i => $jawaban) {
				$array['datas'][$i] = (object) [
					'question'	=> $jawaban->kunci->question,
					'opt_a'		=> $jawaban->kunci->opt_a,
					'opt_b'		=> $jawaban->kunci->opt_b,
					'opt_c'		=> $jawaban->kunci->opt_c,
					'opt_d'		=> $jawaban->kunci->opt_d,
					'kunci'		=> $jawaban->kunci->opt_ok,
					'jawab'		=> $jawaban->answer,
					'status'	=> $jawaban->answer == $jawaban->kunci->opt_ok	?	1 : 0,
				];
			}
		} elseif ($ulangan->exam_shareAnswer == 0) {
			// return jawaban dan status benar/salah
			foreach ($jawabans as $i => $jawaban) {
				$array['datas'][$i] = (object) [
					'question'	=> $jawaban->kunci->question,
					'opt_a'		=> $jawaban->kunci->opt_a,
					'opt_b'		=> $jawaban->kunci->opt_b,
					'opt_c'		=> $jawaban->kunci->opt_c,
					'opt_d'		=> $jawaban->kunci->opt_d,
					'kunci'		=> null,
					'jawab'		=> $jawaban->answer,
					'status'	=> $jawaban->answer == $jawaban->kunci->opt_ok	?	1 : 0,
				];
			}
		}


		return view('ulangan.review', $array);
	}
}

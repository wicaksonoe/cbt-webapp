<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Profile;

class MainController extends Controller
{
	/*	
		Function to check existing session
		If not available return to Login Page
	*/
    public function index()
	{
		$data = Auth::user();
		if(isset($data)){
			return redirect()->route('dashboard.index');
		} else {
			return view('main.main');
		}
	}


	/*	
		Function to try Login Credentials
	*/
	public function login(Request $request)
	{
		$rules = [
			'username' => 'required',
			'password' => 'required'
		];

		$message = [
			'username.required' => 'Kolom username wajib diisi.',
			'password.required' => 'Kolom password wajib diisi.',
		];

		Validator::make($request->all(), $rules, $message)->validate();

		if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
			return redirect()->route('dashboard.index');
		} else {
			return back()->with('error', 'Kombinasi username dan password tidak cocok.');
		}
	}


	/*
		Function Logout
	*/
	public function logout()
	{
		Auth::logout();
		return redirect()->route('main.index');
	}

	/*	
		Function to return Register Page
	*/
	public function register()
	{
		return view('main.register');
	}


	/*	
		Function to check ajax request
		And check username availability
	*/
	public function check(Request $request)
	{
		if ($request->ajax()) {
			$stts = User::where('username', $request['nama'])->first();
			if ($stts) {
				return 'TRUE';
			} else {
				return 'FALSE';
			}
		} else {
			Auth::logout();
			return redirect()->route('main.index');
		}
	}


	/*	
		Function to create new user
	*/
	public function store(Request $request)
	{
		$rules = [
			'username'		=> 'bail|required|min:4|unique:users',
			'password'		=> 'bail|required|min:4',
			're_password'	=> 'bail|required|same:password',
			'level' 		=> 'bail|required|boolean'
		];

		$message = [
			'username.required'		=> 'Kolom username harus diisi.',
			'username.min'			=> 'Kolom username minimal 4 karakter.',
			'username.unique'		=> 'Username sudah dipakai, silahkan klik cek sebelum submit.',
			'password.required'		=> 'Kolom password harus diisi.',
			'password.min'			=> 'Kolom password minimal 4 karakter.',
			're_password.required'	=> 'Kolom re-password harus diisi.',
			're_password.same'		=> 'Kolom re-password harus sama dengan password.',
			'level.required'		=> 'Silahkan pilih Guru / Murid',
			'level.boolean'			=> 'Silahkan pilih Guru / Murid',
		];

		Validator::make($request->all(), $rules, $message)->validate();

		// Post new user to Users Table
		$newUser = new User();
		$newUser->username	= $request->username;
		$newUser->password	= Hash::make($request->password);
		$newUser->level		= $request->level;
		$newUser->save();
		
		// Post new user Profile to Profiles Table based from user_id
		$newProfile = new Profile();
		$newProfile->user_id = $newUser->id;
		$newProfile->save();
		
		return redirect()->route('main.index')->with('success', 'Registrasi berhasil silahkan Login.');
	}
}

<?php

namespace App\Http\Controllers;

use Image;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Exam;
use App\Question;

use App\Exports\QuestionExport;
use App\Imports\QuestionImport;
use Maatwebsite\Excel\Facades\Excel;

class SoalController extends Controller
{
	// Display Soal Dashboard
	public function index()
	{
		// Load data to pass for view
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$data_ = [
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];

		if(isset($data)){
			switch ($data->level) {
				case 0:
					$ulangan	= Exam::where('user_id', $data->id)->get();
					$data_['ulangan'] = $ulangan;

					return view('dashboard.admin-soal', $data_);	
					break;
				
				case 1:
					abort(401);
					break;

				default:
					Auth::logout();
					abort(401);
					break;
			}
		}
	}

	// Load soal
	public function soalLoad(Request $request)
	{
		$soal_ = array();
		if ($request->ajax()) {
			if(!is_numeric($request->id)) {
				$soal_['status'] = 'failed';
				return $soal_;
			} else {
				$soal = Exam::where('id', $request->id)->first()->soal;
				if ($soal->count() > 0) {
					foreach ($soal as $key => $value) {
						$soal_['data'][$key] = $value;
					}
					$soal_['status'] = 'success';
				} else {
					$soal_['status'] = 'failed';
				}
				return $soal_;
			}
			
		} else {
			abort(401);
		}
	}

	// Send New Soal to dB
	public function soalStore(Request $request)
	{
		if ($request->ajax()) {
			// Request validation

			$rules_1 = ['exam_id' => 'required'];

			$rules_2 = [
				'question'	=> 'bail|required|unique:questions',
				'opt_ok'	=> 'bail|required|size:1'
			];

			$messages = [
				'exam_id.required'		=> 'Silahkan pilih judul ulangan terlebih dahulu',
				'question.required'		=> 'Mohon isi kolom <strong>soal</strong>',
				'question.unique'		=> 'Soal sudah ada, silahkan buat soal baru.',
				'opt_ok.required'		=> 'Mohon isi kolom <strong>kunci jawaban</strong>'
			];
			
			$validator_1 = Validator::make($request->all(), $rules_1, $messages);
			if($validator_1->fails()) {
				return response()->json([
					'status'=>"failed",
					'value'=>'0',
					'errors'=>$validator_1->errors()->all()
					]);
			}
				
			$validator_2 = Validator::make($request->all(), $rules_2, $messages);
			if($validator_2->fails()) {
				return response()->json([
						'status'=>"failed",
						'value'=>'0',
						'errors'=>$validator_2->errors()->all()
					]);
			}

			$soal = new Question();
			
			$soal->exam_id	= $request->exam_id;
			$soal->question	= $request->question;
			$soal->opt_a	= $request->opt_a;
			$soal->opt_b	= $request->opt_b;
			$soal->opt_c	= $request->opt_c;
			$soal->opt_d	= $request->opt_d;
			$soal->opt_ok	= $request->opt_ok;
			$soal->save();

			$return['status'] = "success";
			$return['value'] = 1;
			$return['data']['exam_id'] = $request->exam_id;
			
			return $return;
		} else {
			abort(401);
		}
	}


	// View Details & Open Edit Form
	public function soalEdit(Request $request)
	{
		if ($request->ajax()) {
			$soal = Question::where('id', $request->id);
			if ($soal->count() == 1) {
				$return = [
					'status' 	=> 'success',
					'value'		=> '1',
					'data'		=> $soal->first()
				];
			} else {
				$return = [
					'status' 	=> 'failed',
					'value'		=> '0'
				];
			}
			return $return;
		} else {
			abort(401);
		}
	}


	// Update Existing Soal
	public function soalUpdate(Request $request)
	{
		if ($request->ajax()) {
			$data = Question::where('id', $request->id)->first();
			if(Question::where('id', $request->id)->count() == 1) {
				(!isset($request->question)	? 	: $data->question 	= $request->question);
				(!isset($request->opt_a)	? 	: $data->opt_a	 	= $request->opt_a);
				(!isset($request->opt_b)	? 	: $data->opt_b	 	= $request->opt_b);
				(!isset($request->opt_c)	? 	: $data->opt_c	 	= $request->opt_c);
				(!isset($request->opt_d)	? 	: $data->opt_d	 	= $request->opt_d);
				$data->opt_ok 	= $request->opt_ok;
				$data->save();
				
				$return = [
					'value' => 1,
					'status' => 'success'
				];

				return $return;
			} else {
				$return = [
					'value' => 0,
					'status' => 'failed'
				];
				
				return $return;
			}
		} else {
			abort(401);
		}
	}


	// Delete Selected Soal
	public function soalDelete(Request $request)
	{
		if ($request->ajax()) {
			$data = Question::where('id', $request->id);
			if ($data->count() == 1) {
				$data->delete();
				$return = [
					'value' => 1,
					'status' => 'success'
				];
				return $return;
			} else {
				$return = [
					'value' => 0,
					'status' => 'failed'
				];
				return $return;
			}
		} else {
			abort(401);
		}
	}

	// Funtion to Export soal
	public function exportSoal(Request $request)
	{
		if(isset($request->id) && isset($request->exam_name)) {
			return (new QuestionExport((int)$request->id))->download('soal-'.$request->exam_name.'.xlsx');
		}

		return redirect()->route('dashboard.soal');
	}
	
	// Funtion to Import soal
	public function importSoal(Request $request)
	{
		$rules = [
			'import_soal'	=> 'bail|nullable|mimes:xls,xlsx',
		];

		$message = [
			'import_soal.mimes' => 'Hanya menerima format xls / xlsx. Silahkan klik export soal terlebih dahulu.'
		];

		Validator::make($request->all(), $rules, $message)->validate();

		Excel::import(new QuestionImport, request()->file('import_soal'));
		return redirect()->back();
	}
}

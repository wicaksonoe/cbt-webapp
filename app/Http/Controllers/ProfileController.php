<?php

namespace App\Http\Controllers;

use Image;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Profile;
use App\User;

class ProfileController extends Controller
{
	// Show User Profile
	public function index()
	{
		$data = Auth::user();
		$profile = Profile::where('user_id', $data->id)->first();

		if(isset($data)){
			switch ($data->level) {
				case 0:
					$data = [
						'id'			=> $data->id,
						'firstName'		=> $profile->nama_depan,
						'lastName'		=> $profile->nama_belakang,
						'username'		=> $data->username,
						'kelas'			=> $profile->kelas,
						'posisi'		=> $profile->posisi,
						'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
						'photo_profile'	=> Image::make($profile->photo_profile)->resize(110, 110)->encode('data-url'),
						'photo_cover' 	=> $profile->photo_cover
					];
					return view('dashboard.admin-profile', $data);
					break;
				
				case 1:
					$data = [
						'id'			=> $data->id,
						'firstName'		=> $profile->nama_depan,
						'lastName'		=> $profile->nama_belakang,
						'username'		=> $data->username,
						'kelas'			=> $profile->kelas,
						'posisi'		=> $profile->posisi,
						'no_absen'		=> $profile->no_absen,
						'nis'			=> $profile->nis,
						'nisn'			=> $profile->nisn,
						'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
						'photo_profile'	=> Image::make($profile->photo_profile)->resize(110, 110)->encode('data-url'),
						'photo_cover' 	=> $profile->photo_cover
					];
					return view('dashboard.user-profile', $data);
					break;

				default:
					Auth::logout();
					abort(401);			
					break;
			}
		}
	}

	// Update User Profile
	public function userProfileUpdate(Request $request)
	{
		$data		= Auth::user();
		$profile_	= Profile::where('user_id', $data->id)->first();
		$user_		= User::find($data->id);

		$rules = [
			'name_first'	=> 'bail|nullable|min:4',
			'no_absen'		=> 'bail|nullable|numeric|min:1',
			'nis'			=> 'bail|nullable|numeric|min:1',
			'nisn'			=> 'bail|nullable|numeric|min:1',
			'photo_profile'	=> 'bail|nullable|image|mimes:jpg,jpeg',
			'photo_cover'	=> 'bail|nullable|image|mimes:jpg,jpeg',
			'password'		=> 'bail|nullable|min:4',	
			're_password'	=> 'bail|nullable|same:password',	
		];

		$message = [
			'name_first.min'		=> 'Kolom nama depan minimal 4 karakter.',

			'no_absen.numeric'		=> 'Kolom absen harus berupa angka.',
			'no_absen.min'			=> 'Masukan absen yang valid.',
			
			'nis.numeric'			=> 'Kolom NIS harus berupa angka.',
			'nis.min'				=> 'Masukan NIS yang valid.',
			
			'nisn.numeric'			=> 'Kolom NISN harus berupa angka.',
			'nisn.min'				=> 'Masukan NISN yang valid.',

			'password.min'			=> 'Password baru minimal 4 karakter.',
			're_password.same'		=> 'Re-password harus sama dengan password baru.',
			
			'photo_profile.image'	=> 'Kolom foto hanya menerima file JPG / JPEG.',
			'photo_profile.mimes'	=> 'Kolom foto hanya menerima file JPG / JPEG.',

			'photo_cover.image'		=> 'Kolom foto hanya menerima file JPG / JPEG.',
			'photo_cover.mimes'		=> 'Kolom foto hanya menerima file JPG / JPEG.',
		];

		Validator::make($request->all(), $rules, $message)->validate();
		
		if ( $request->file('photo_profile') !== null) {
			$image_profile	= Image::make($request->file('photo_profile'))->resize(null, 300, function($constraint) {
									$constraint->aspectRatio();
								})->crop(300, 300)->save('storage/images/profile-'.$data->id.'.jpg');

			$profile_->photo_profile = 'storage/images/profile-'.$data->id.'.jpg';
			$profile_->save();
		}

		if ( $request->file('photo_cover') !== null) {
			$image_cover	= Image::make($request->file('photo_cover'  ))->crop(1200, 300)->save('storage/images/cover-'.$data->id.'.jpg');

			$profile_->photo_cover = 'storage/images/cover-'.$data->id.'.jpg';
			$profile_->save();
		}

		switch ($data->level) {
			case 0:
				// Ternary operator to check if $_POST NOT NULL, UPDATE DATA, Else DO NOTHING.
				(!isset($request->name_first)				? 	: $profile_->nama_depan 	= $request->name_first );
				(!isset($request->name_last)				?	: $profile_->nama_belakang 	= $request->name_last);
				(!isset($request->kelas)					?	: $profile_->kelas			= $request->kelas);
				(!isset($request->posisi)					?	: $profile_->posisi			= $request->posisi);
				$profile_->save();

				// Ternary operator to check if $_POST NOT NULL, UPDATE DATA, Else DO NOTHING.
				(!isset($request->password)			?	: $user_->password			= Hash::make($request->password));
				$user_->save();

				return redirect()->route('profile.index');
				break;
			
			case 1:
				// Ternary operator to check if $_POST NOT NULL, UPDATE DATA, Else DO NOTHING.
				(!isset($request->name_first)				? 	: $profile_->nama_depan 	= $request->name_first);
				(!isset($request->name_last)				?	: $profile_->nama_belakang 	= $request->name_last);
				(!isset($request->kelas)					?	: $profile_->kelas			= $request->kelas);
				(!isset($request->posisi)					?	: $profile_->posisi			= $request->posisi);
				(!isset($request->no_absen)					?	: $profile_->no_absen		= $request->no_absen);
				(!isset($request->nis)						?	: $profile_->nis			= $request->nis);
				(!isset($request->nisn)						?	: $profile_->nisn			= $request->nisn);
				$profile_->save();

				// Ternary operator to check if $_POST NOT NULL, UPDATE DATA, Else DO NOTHING.
				(!isset($request->password)		?	: $user_->password			= Hash::make($request->password));
				$user_->save();

				return redirect()->route('profile.index');
				break;

			default:
				Auth::logout();
				return redirect()->route('main.index');
				break;
		}
	}
}

<?php

namespace App\Http\Controllers;

use Image;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Exam;
use App\Report;
use App\Profile;

use App\Exports\ReportsExport;
use App\Exports\NilaiSiswaExport;
use Maatwebsite\Excel\Facades\Excel;

class NilaiController extends Controller
{
	// Display Nilai Dashboard based on level User
	public function index()
	{
		// Load data to pass for view
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$data_ = [
			'id'		=> $data->id,
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];

		if(isset($data)){
			switch ($data->level) {
				case 0:
					$data_['ulangan'] = Exam::where('user_id', $data->id)->get();		
					return view('dashboard.admin-nilai', $data_);
					break;
				
				case 1:
					$data_['nilai'] = $this->userLoadNilai($data->id);
					return view('dashboard.user-nilai', $data_);
					break;
				
				default:
					Auth::logout();
					abort(401);			
					break;
			}
		}
	}

	public function userLoadNilai($user_id)
	{
		$reports = Report::where('user_id', $user_id)->get();
		
		if (count($reports) == 0) {
			return NULL;
		}
		foreach ($reports as $key => $report) {
			$return[$key] = (object)[
				'point'		=> $report->point,
				'kkm'		=> $report->exam->exam_kkm,
				'exam_name'	=> $report->exam->exam_name,
				'exam_id'	=> $report->exam_id,
				'status'	=> $report->point >= $report->exam->exam_kkm ? 'LULUS' : 'TIDAK LULUS'
			];
		}
		
		return $return;
	}

	// Load nilai and return result as JSON
	public function nilaiLoad(Request $request)
	{
		if ($request->ajax()) {
			$data = Report::where('exam_id', $request->id);

			if($data->count() > 0) {
				$nilai = $data->select('user_id', 'point')->get();
				foreach ($nilai as $key => $item) {
					$return['data'][$key] = [
						'nilai'			=> $item->point,
						'nama_depan'	=> $item->profile->nama_depan, 
						'nama_belakang'	=> $item->profile->nama_belakang,
						'no_absen'		=> $item->profile->no_absen,
						'kelas'			=> $item->profile->kelas, 
						'posisi'		=> $item->profile->posisi
					];
				}

				$return['value'] = 1;
				$return['status'] = 'success';

				return $return;
			} else {
				$return = [
					'value' => 0,
					'status' => 'failed'
				];
				
				return $return;
			}
		} else {
			abort(401);
		}
	}

	// Nilai export
	public function nilaiExport(Request $request)
	{
		$user = Auth::user();
		$profile = Profile::where('user_id', $user->id)->first();
		if(isset($user)) {
			if($user->level == 0) {
				return (new ReportsExport($request->id))->download('Nilai-'.str_slug($request->exam_name, '_').'.xlsx');
			} else if($user->level == 1) {
				return (new NilaiSiswaExport($request->id))->download('Nilai-'.str_slug($profile->nama_depan.' '.$profile->nama_belakang, '_').'.xlsx');
			}
		}
		abort(401);
	}

}

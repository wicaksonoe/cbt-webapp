<?php

namespace App\Http\Controllers;

use Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class DashboardController extends Controller
{

	// Display Main Dashboard based on level User
    public function index()
	{
		// Load data to pass for view
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$data_ = [
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];

		if(isset($data)){
			switch ($data->level) {
				case 0:
					return view('dashboard.admin-dash', $data_);	
					break;
				
				case 1:
					return view('dashboard.user-dash', $data_);	
					break;
				
				default:
					Auth::logout();
					abort(401);			
					break;
			}
		}
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Image;
use App\Quote;
use App\User;
use Illuminate\Support\Facades\Auth;

class QuoteController extends Controller
{
    public function index()
	{
		$data = Auth::user();
		$profile = User::find($data->id)->profile;
		$data_ = [
			'userID'	=> $data->id,
			'firstName' => $profile->nama_depan,
			'lastName'	=> $profile->nama_belakang,
			'kelas'		=> $profile->kelas,
			'posisi'	=> $profile->posisi,
			'small_profile'	=> Image::make($profile->photo_profile)->resize(48, 48)->encode('data-url'),
		];

		if(isset($data)){
			switch ($data->level) {
				case 0:	
					return view('dashboard.admin-quote', $data_);
					break;
				
				case 1:					
					abort(401);
					break;

				default:
					Auth::logout();
					abort(401);			
					break;
			}
		}
	}

	public function createQuote(Request $request)
	{
		$rules = [
			'quote_content'	=> 'bail|required',
			'quote_by'		=> 'bail|required',
		];

		$messages = [
			'quote_content.required'	=> 'Kolom quote konten harus diisi.',
			'quote_by.required'			=> 'Kolom quote by harus diisi.',
		];

		$validate = Validator::make($request->all(), $rules, $messages);

		if($validate->fails()) {
			$return = [
				'value' => 0,
				'status' => 'failed',
				'errors' => $validate->getMessageBag()->toArray()
			];
			
			return $return;
		}

		$newQuote = new Quote();
		$newQuote->user_id			= Auth::user()->id;
		$newQuote->quote_content	= $request->quote_content;
		$newQuote->quote_by			= $request->quote_by;
		$newQuote->save();

		$return = [
			'value' => 1,
			'status' => 'success',
		];
		
		return $return;
	}

	public function readQuote(Request $request)
	{
		switch ($request->mode) {
			case 0:
				$quotes = Quote::where('user_id', Auth::user()->id)->get();

				if( count($quotes) > 0 ) {
					$return['value'] = 1;
					foreach ($quotes as $key => $quote) {
						$return['data'][$key] = [
							'quote_id'		=> $quote->id,
							'quote_content'	=> $quote->quote_content,
							'quote_by'		=> $quote->quote_by,
						];
					}
				} else {
					$return['value'] = 0;
				}
				break;

			case 1:
				$quote = Quote::find($request->quote_id);

				if ( $quote ) {
					$return = [
						'value'	=> 1,
						'data'	=> [
							'quote_id'		=> $quote->id,
							'quote_content'	=> $quote->quote_content,
							'quote_by'		=> $quote->quote_by,
						]
					];
				} else {
					$return['value'] = 0;
				}
				break;
				
			default:
				$return['value'] = 0;
				break;
		}

		return $return;
	}

	public function updateQuote(Request $request)
	{
		$selectedQuote = Quote::find($request->quote_id);
		
		$return['value'] = 0;

		if ($selectedQuote->user_id == Auth::user()->id) {
			$selectedQuote->quote_content	= $request->quote_content;
			$selectedQuote->quote_by		= $request->quote_by;
			$selectedQuote->save();

			$return['value'] = 1;
		}
		
		return $return;
	}

	public function deleteQuote(Request $request)
	{
		$selectedQuote = Quote::find($request->quote_id);

		$return['value'] = 0;

		if ( $selectedQuote ) {
			if ($selectedQuote->user_id == Auth::user()->id) {
				$selectedQuote->delete();
	
				$return['value'] = 1;
			} 
		}
		
		return $return;
	}
}

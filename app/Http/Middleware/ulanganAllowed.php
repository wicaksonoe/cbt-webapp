<?php

namespace App\Http\Middleware;

use Closure;
use App\Profile;
use App\Report;
use App\Exam;
use Illuminate\Support\Facades\Auth;

class ulanganAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

	private static function allowedClass(Int $ulanganId)
	{
		$id = Auth::user()->id;
		
		$kelas = Profile::where('user_id', $id)->first()->kelas;
		$ulanganKelas = Exam::find($ulanganId)->exam_class;

		if($kelas == $ulanganKelas) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private static function allowedTime(Int $ulanganId)
	{
		$id = Auth::user()->id;
		
		$ulanganTime = Exam::find($ulanganId)->exam_time * 60;
		$user = Report::where('user_id', $id)->where('exam_id', $ulanganId)->select('created_at');
		
		if ($user->count() == 0) {
			return TRUE;
		}

		$elapsedTime = time() - strtotime($user->first()->created_at);

		if($elapsedTime < $ulanganTime) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private static function allowedStatus(Int $ulanganId)
	{
		if (Exam::find($ulanganId)->exam_status == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	private static function allowedAttempt(Int $ulanganId)
	{
		$id = Auth::user()->id;
		$report = Report::where('user_id', $id)->where('exam_id', $ulanganId);
		if ($report->count() == 0 || $report->first()->status == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    public function handle($request, Closure $next)
    {
		if ( self::allowedClass($request->id) && self::allowedTime($request->id) && self::allowedStatus($request->id) && self::allowedAttempt($request->id) ) {
			return $next($request);
		} else if ( !self::allowedTime($request->id) || !self::allowedAttempt($request->id) ) {
			if ($request->ajax()) {
				return $next($request);
			} else {
				return redirect()->action(
					'UjianController@ujianSelesai', [
						'id' => $request->id
					]
				);
			}
		} else {
			return redirect('/dashboard/ulangan')->with('error', 'Maaf anda tidak dapat mengikuti ulangan yang anda pilih.');
		}
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	public function profile() {
		return $this->belongsTo('App\Profile', 'user_id', 'user_id');
	}

	public function exam()
	{
		return $this->belongsTo('App\Exam', 'exam_id', 'id');
	}
}

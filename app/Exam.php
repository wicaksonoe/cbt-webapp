<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function soal()
	{
		return $this->hasMany('App\Question');
	}
}

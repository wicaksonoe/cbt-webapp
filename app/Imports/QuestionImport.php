<?php

namespace App\Imports;

use App\Question;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class QuestionImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Question([
			'exam_id'	=> $row[str_slug('KODE ULANGAN', '_')],
            'question'	=> $row[str_slug('Soal / Pertanyaan', '_')],
            'opt_a'		=> $row[str_slug('Opsi A', '_')],
            'opt_b'		=> $row[str_slug('Opsi B', '_')],
            'opt_c'		=> $row[str_slug('Opsi C', '_')],
            'opt_d'		=> $row[str_slug('Opsi D', '_')],
            'opt_ok'	=> $row[str_slug('Kunci (A / B / C / D)', '_')]
        ]);
    }
}

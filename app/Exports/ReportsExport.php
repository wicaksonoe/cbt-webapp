<?php

namespace App\Exports;

use App\Report;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportsExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
	use Exportable;

	public function __construct(int $id) {
		$this->id = $id;
	}

	public function query() {
		return Report::where('exam_id', $this->id);
	}

	public function map($data): array
	{
		return [
			$data->profile->nama_depan.' '.$data->profile->nama_belakang,
			$data->profile->kelas.$data->profile->posisi,
			$data->profile->no_absen,
			$data->point
		];
	}

	public function headings(): array
	{
		return [
			'Nama',
			'Kelas',
			'No. Absen',
			'Nilai'
		];
	}
}
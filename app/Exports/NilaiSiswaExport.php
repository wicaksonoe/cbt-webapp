<?php

namespace App\Exports;

use App\Report;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class NilaiSiswaExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
	use Exportable;

	public function __construct(int $id) {
		$this->id = $id;
	}

	public function query() {
		return Report::where('user_id', $this->id);
	}

	public function map($data): array
	{
		return [
			$data->exam->exam_name,
			$data->point,
			$data->exam->exam_kkm,
			$data->point >= $data->exam->exam_kkm ? 'LULUS' : 'TIDAK LULUS'
		];
	}

	public function headings(): array
	{
		return [
			'Judul Ulangan',
			'Perolehan Nilai',
			'KKM',
			'Status'
		];
	}
}
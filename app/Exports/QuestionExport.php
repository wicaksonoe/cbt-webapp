<?php

namespace App\Exports;

use App\Question;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class QuestionExport implements FromQuery, WithHeadings, ShouldAutoSize
{
	use Exportable;
	
	public function __construct(int $id)
	{
		$this->id = $id;
	}

	public function headings(): array
	{
		return [
			'KODE ULANGAN',
			'Soal / Pertanyaan',
			'Opsi A',
			'Opsi B',
			'Opsi C',
			'Opsi D',
			'Kunci (A / B / C / D)',
		];
	}

    public function query()
    {
        return Question::query()->select('exam_id', 'question', 'opt_a', 'opt_b', 'opt_c', 'opt_d', 'opt_ok')->where('exam_id', $this->id);
    }
}

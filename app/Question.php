<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $hidden = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'exam_id',
		'question',
		'opt_a',
		'opt_b',
		'opt_c',
		'opt_d',
		'opt_ok'
	];
	
	public function jawaban()
	{
		return $this->hasMany('App\Answer', 'question_id', 'id');
	}
}

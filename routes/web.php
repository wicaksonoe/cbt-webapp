<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
	Login Register Dashboar Route
*/
// Route main page
Route::get('/', 'MainController@index')->name('main.index');

// Route Try Login
Route::post('/login', 'MainController@login')->name('main.login');

// Route logout
Route::get('/logout', 'MainController@logout')->name('logout');

// Route register page
Route::get('/register', 'MainController@register')->name('register.index');

// Route Try register
Route::post('/register', 'MainController@store')->name('register.store');

// Route Username Check    # AJAX ONLY
Route::get('/check', 'MainController@check');

Route::group(['middleware' => ['auth']], function () {
	// Route List to manage Profile
	Route::group(['prefix' => 'user'], function () {
		Route::get('profile', 'ProfileController@index')->name('profile.index');
		Route::put('profile', 'ProfileController@userProfileUpdate')->name('profile.update');
	});
	
	// Route List to attemp ulangan
	Route::group(['prefix' => 'ulangan'], function () {
		Route::get('try/{id}', 'UjianController@index')->middleware('ulanganAllowed');
		Route::put('try/{id}', 'UjianController@putAnswer')->middleware('isAjax', 'ulanganAllowed');
		Route::get('done/{id}', 'UjianController@ujianSelesai');
		Route::get('review/{id}', 'UjianController@ujianReview');
	});


	// Route List to manage dashboard
	Route::group(['prefix' => 'dashboard'], function () {
		// Route to dashboard main page
		Route::get('/', 'DashboardController@index')->name('dashboard.index');
		
		// Route List to manage quote
		Route::group(['prefix' => 'quote'], function () {
			Route::get('/', 'QuoteController@index')->name('dashboard.quote')->middleware( 'isGuru');
			Route::post('/', 'QuoteController@createQuote')->middleware( 'isAjax', 'isGuru');
			Route::put('/', 'QuoteController@updateQuote')->middleware( 'isAjax', 'isGuru');
			Route::delete('/', 'QuoteController@deleteQuote')->middleware( 'isAjax', 'isGuru');
			Route::get('get', 'QuoteController@readQuote')->middleware( 'isAjax', 'isGuru');
		});
			
		// Route List to manage Nilai
		Route::group(['prefix' => 'nilai'], function () {
			Route::get('/', 'NilaiController@index')->name('dashboard.nilai');
			Route::get('load', 'NilaiController@nilaiLoad');
			Route::get('export', 'NilaiController@nilaiExport');
		});
		
		// Route List to manage Ulangan
		Route::group(['prefix' => 'ulangan'], function () {
			Route::get('/', 'UlanganController@index')->name('dashboard.ulangan');
			Route::post('submit', 'UlanganController@ulanganStore')->name('dashboard.ulangan.store')->middleware( 'isGuru');
			Route::post('toggle', 'UlanganController@toggleStatus')->middleware( 'isGuru');
			Route::post('share', 'UlanganController@shareUlanganKunci')->middleware('isAjax',  'isGuru');
			Route::post('update', 'UlanganController@updateUlangan')->middleware( 'isGuru');
			Route::delete('delete', 'UlanganController@deleteUlangan')->middleware( 'isGuru');
			
			// Route List to monitor Ulangan
			Route::group(['prefix' => 'monitor'], function () {
				Route::get('/', 'UlanganController@ulanganMonitoring')->name('dashboard.ulangan.monitor')->middleware( 'isGuru');
				Route::post('/', 'UlanganController@ulanganForceFinish')->middleware('isAjax',  'isGuru');
				Route::get('get', 'UlanganController@ulanganMonitoringGet')->middleware('isAjax',  'isGuru');
			});
		});

		// Route List to manage soal
		Route::group(['prefix' => 'soal'], function () {
			Route::get('/', 'SoalController@index')->name('dashboard.soal');
			Route::post('load', 'SoalController@soalLoad');
			Route::post('store', 'SoalController@soalStore')->middleware( 'isGuru');
			Route::post('edit', 'SoalController@soalEdit')->middleware( 'isGuru');
			Route::put('update', 'SoalController@soalUpdate')->middleware( 'isGuru');
			Route::delete('delete', 'SoalController@soalDelete')->middleware( 'isGuru');
			Route::get('export', 'SoalController@exportSoal')->middleware( 'isGuru');
			Route::post('import', 'SoalController@importSoal')->middleware( 'isGuru');
		});
	});
});